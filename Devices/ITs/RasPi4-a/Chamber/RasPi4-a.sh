#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh

RASP=Chamber
SSHatRASP="ssh -Y golem@$RASP"
SOURCE='source Rasp-VacuumLog.sh;source Rasp-WorkingGas.sh'

# Device service for the GOLEM operation (kotvy/anchors) START
##############################################################

function PrepareSessionEnv@SHM()
{
    scp Rasp-*.sh $RASP:
    scp $SW/Commons.sh $RASP:
    scp $SW/Devices/ITs/Drivers/Rasp-Commons.sh $RASP:
    # $SSHatRASP "$SOURCE;PrepareSessionEnv@SHM" #maybe ..
    
    ssh chamber "source Rasp-VacuumLog.sh;PrepareSessionEnv@SHM"

}

function CallRasp() #maybe ..
{
    $SSHatRASP "$SOURCE;$1"
}

function VacuumLog()
{
    $SSHatRASP "$SOURCE;VacuumLog"
}

