#!/bin/bash

# To be executed at RASPs

source Rasp-Commons.sh

source Drivers/TPG262GNOME232/driver.sh
source Drivers/PapaGo2TC_ETH/driver.sh


# Device service for the GOLEM operation (kotvy/anchors) START
##############################################################


function PrepareSessionEnv@SHM()
{
    MountCentralSHMEnvironment 
}


function ReadChamberTemp()
{
    PapagoReadCh1
}



function VacuumLog()
{
    echo -ne NULL > $SHML/GasSwitch;
    echo -ne 0 > $SHML/ActualVoltageAtGasValve;
    rm -f $SHM/ChamberLog 
    rm -f $SHML/GlobalLogbook

    while [ 1 ]; do
        for i in `seq 1 5`; do
        #1122 Bug fix, channels switched START
            #ActualChamberPressurePa=`get_chamber_pressure`
            #ActualForVacuumPressurePa=`get_forvacuum_pressure`
            ActualChamberPressurePa=`get_forvacuum_pressure`
            ActualForVacuumPressurePa=`get_chamber_pressure`
        #1122 Bug fix, channels switched END
            ActualChamberTemperature=`PapagoReadCh1`
            #echo $ActualChamberPressurePa > $SUBDIR/$ThisDev/ActualChamberPressurePa #NEJDE, ODSTRELUJE sshfs
            echo -ne $ActualChamberPressurePa > $SHML/ActualChamberPressurePa
            echo -ne  $ActualForVacuumPressurePa > $SHML/ActualForVacuumPressurePa
            echo -ne  $ActualChamberTemperature > $SHML/ActualChamberTemperature
            echo -ne  `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'|sed 's/,/\./g'` > $SHML/ActualChamberPressuremPa
            echo `date '+%H:%M:%S'` "Chamber: `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'` mPa, ForVacuum: $ActualForVacuumPressurePa Pa, Gas: `cat $SHML/GasSwitch`, GasValve: `cat $SHML/ActualVoltageAtGasValve|xargs printf '%4.2f'` V">> $SHML/PressureLog
            echo `date '+%H:%M:%S'` " " `echo $ActualChamberPressurePa*1000|bc|xargs printf '%4.2f'` " " `cat $SHML/ActualChamberTemperature|xargs printf '%4.2f'`  >> $SHM/ChamberLog
            #sleep 1 # neni potreba, je to zatim pomale dost
        done
        LogIt "\
`echo $ActualChamberPressurePa*1000|bc|xargs printf 'p_ch=%4.2f'|sed 's/,/\./g'`\
`if [[ $(<$SHMS/SessionLogBook/tokamak_state) != 'idle' ]]; then echo -ne '/';cat $SHM0/Infrastructure/WorkingGas/Hydrogen/Parameters/p_h;fi`\
 mPa,\
`cat $SHML/ActualChamberTemperature|xargs printf 'T_ch=%4.1f'` C,\
`cat $SHML/ActualForVacuumPressurePa|xargs printf 'p_fv=%3.1f'` Pa,\
Gas: `cat $SHML/GasSwitch`,\
`cat $SHML/ActualVoltageAtGasValve|xargs printf 'U_wg=%3.1f'` V";
    done
}	



#Developing issues
#psa|grep VacuumLog|awk '{print $2}'|xargs kill
