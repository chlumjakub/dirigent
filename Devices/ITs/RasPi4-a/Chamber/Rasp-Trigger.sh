#!/bin/bash

# To be executed at RASPs

source Rasp-Commons.sh


source Drivers/Arduino8relayModul/driver.sh


function HWTrigger()
{
# Back compatibility ..
local SUBDIR=RASPs
local ThisDev=Discharge

    mkdir -p $SHM0/Devices/$SUBDIR/$ThisDev

    $LogFunctionGoingThrough
    
    #Tch3_request=`cat $SHMP/TBt` (0 nefunguje ????, musí být min 1, vyresit pres if)
    if [ -f "$SHM0/Infrastructure/Bt_Ecd/Parameters/t_bt" ]; then
        Tch3_request=$(echo `cat $SHM0/Infrastructure/Bt_Ecd/Parameters/t_bt`|bc) # workarround
    else 
         Tch3_request=1
    fi
    echo $Tch3_request > $SHM0/Operation/Discharge/t_bt_discharge_request
    echo $Tch3_request > $SHM0/Operation/Discharge/Tch3_request
    LogIt "Going with trigger Tch3 - TBt $Tch3_request"

    if [ -f "$SHM0/Infrastructure/Bt_Ecd/Parameters/t_cd" ]; then
        Tch4_request=$(echo `cat $SHM0/Infrastructure/Bt_Ecd/Parameters/t_cd`-`cat $SHM0/Infrastructure/Bt_Ecd/Parameters/t_bt`|bc)
    else 
         Tch4_request=1
    fi
    echo $Tch4_request > $SHM0/Operation/Discharge/t_cd_discharge_request
    echo $Tch4_request > $SHM0/Operation/Discharge/Tch4_request
    LogIt "Going with trigger Tch4 - Tcd $Tch4_request"

   
    
    echo "void setup() {                
  pinMode(2, OUTPUT);                                                                                                                                                                                                                         
  pinMode(3, OUTPUT);                                                                                                                                                                                                                         
  pinMode(4, OUTPUT);                                                                                                                                                                                                                         
  pinMode(5, OUTPUT);                                                                                                                                                                                                                         
  pinMode(6, OUTPUT);                                                                                                                                                                                                                         
  pinMode(7, OUTPUT);
  // DAS
  digitalWrite(2, HIGH);   
  // system is not working properly for 0 request, 1 us delay (everywhere) is not a problem .. for now.
  delayMicroseconds($((Tch3_request+1))); 
  // Bt
  digitalWrite(3, HIGH);   
  delayMicroseconds($((Tch4_request+1))); 
  // Et
  digitalWrite(4, HIGH);//workarround 1122
  delay(100);
  digitalWrite(2, LOW);   
  digitalWrite(3, LOW);   
  digitalWrite(4, LOW);   
  digitalWrite(5, LOW);   
  digitalWrite(6, LOW);   
  digitalWrite(7, LOW);   
}
void loop() {
}
  " > main.ino       
    
    cp main.ino ~/Drivers/Arduino/TriggerOverUSB/
    make -C ~/Drivers/Arduino/TriggerOverUSB upload 1>/dev/null 2>/dev/null

}



function Trigger()
{
    $LogFunctionGoingThrough
    HWTrigger
}


function TriggerTestInfinity()
{
    EngageBats
    TriggerCloneBatteriesON
    while [ 1 ]; do HWTrigger; echo "Go ..";sleep 1;done
    DisEngageBats
    TriggerCloneBatteriesOFF

}

function TriggerTest100x()
{
    CallCentralFunction $RelayBoards Engage12V_24V_intosystem@Miscellaneous ON
    for i in `seq 1 100`; do HWTrigger; echo "Go ..$i";sleep 1;done
    CallCentralFunction $RelayBoards Engage12V_24V_intosystem@Miscellaneous OFF

}


function TriggerTest1x()
{
    CallCentralFunction $RelayBoards Engage12V_24V_intosystem@Miscellaneous ON
    sleep 1
    for i in `seq 1 1`; do HWTrigger; echo "Go ..$i";sleep 1;done
    CallCentralFunction $RelayBoards Engage12V_24V_intosystem@Miscellaneous OFF
}

function DummyTrigger()
{
    EngageBats
    TriggerCloneBatteriesON
    for i in `seq 1 1`; do HWTrigger; echo "Go ..$i";sleep 1;done
}
