#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh


function Wake-upCall()
{
    LogIt $ThisDev
    shuf -i 0-5|head -1|xargs sleep # To Protect electrical grid
    Call $SW/Infrastructure/Support/Sockets/Tokamak FastCamerasON
}

function Sleep-downCall()
{
    LogIt $ThisDev
    Call $SW/Infrastructure/Support/Sockets/Tokamak  FastCamerasOFF
}


