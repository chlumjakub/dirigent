#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh

function Wake-upCall()
{
    DelayCall $RelayBoards Kepcos4PositionStab@PowerSupplies ON
    sleep 2
    Call $RelayBoards Kepcos@StabilizationON
}

function Sleep-downCall()
{
    DelayCall $RelayBoards Kepcos@StabilizationOFF
    sleep 1
    Call $RelayBoards Kepcos4PositionStab@PowerSupplies OFF
 
}
