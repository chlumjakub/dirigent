#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh


function Wake-upCall()
{
    Call $SHMS/Devices/RelayBoards Manipulator@TokamakSockets ON
    sleep 10
    python3 manipulator_control.py wake
}

function Sleep-downCall()
{
    LogIt $ThisDev
    python3 manipulator_control.py home
    sleep 5
    Call $SHMS/Devices/RelayBoards Manipulator@TokamakSockets OFF
}


function SetPosition()
{
    LogIt $ThisDev
    python3 manipulator_control.py $1
}

function Homing()
{
    LogIt $ThisDev
    python3 manipulator_control.py home &
}

function PingCheck ()
{
    WaitForDevice D-Linkcamera-a $ThisDev
    WaitForDevice D-Linkcamera-b $ThisDev
    WaitForDevice Motor_Controller-a $ThisDev
    WaitForDevice Motor_Controller-b $ThisDev
    

}
