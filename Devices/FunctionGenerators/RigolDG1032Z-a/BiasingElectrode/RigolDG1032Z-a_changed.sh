#!/bin/bash
BasePath=../../../..;source $BasePath/Commons.sh
source ../Universals.sh

COMMAND="netcat -w 1 $ThisDev 5555"




function GetReadyTheDischarge ()
{
mkdir Parameters
    #GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh

    raw=`cat $SHM0/Infrastructure/BiasingElectrode/Basic/Parameters/waveform`
    #raw=`cat $SHM0/Production/Parameters/infrastructure.biasingelectrode`
#    raw=$(python /golem/Dirigent/Infrastructure/BiasingElectrode/Depository/FilPap/biasing_control/asg_control/control_asg_simple.py 3 7 2 1 10)

    raw=$(echo $raw | tr -s ',' ' ')
raw=$(echo $raw | tr -s ';' ' ')

time=()
value=()
a=0
for i in $raw
    do
      if [[ a%2 -eq 0 ]]    
      then
        time+=($i)
      else
        value+=($i)
      fi
    let "a+=1"
    done

echo Time points: ${time[@]}
echo Values: ${value[@]}

sign () { echo "$(( $1 < 0 ? -1 : 1 ))"; } #absolute value

#looks for the max value
Max=0
for m in ${value[@]} 
    do
      m=$(( $m * $(sign "$m") ))    
      if [[ $m -gt $Max ]]
      then 
         Max=$m 
      fi
    done
echo Max value: $Max

#generate sequence of points for the frequency generator
sequence=()
j=0
l=1
for ((i=${time[0]};i<=${time[-1]};i+=100))
   do
     k=${value[$l-1]}
     sequence+=$(python -c "print(round(${k}.0 / ${Max}.0,1),',')") #strange but functional
     if [[ $i -eq ${time[$l]} ]]
     then
        let "l+=1"
     fi
    let "j+=1"
   	done
c=${sequence[@]}
echo $c #raw
VerboseMode Final sequence: #$raw


#  echo ":OUTP2 OFF"|netcat -w 1 192.168.2.171 5555 #Turn off output off ch2
#  sleep 0.1;
#  echo "Waveform pars"
#  echo ":SOUR2:APPL:ARB 10000,$Max,0"|netcat -w 1 192.168.2.171 5555 #Arbitrary 
#  echo ":SOUR2:DATA VOLATILE, $c"|netcat -w 1 192.168.2.171 5555 #predefined function
#  echo "Burst mode"
#  echo ":SOUR2:BURS ON"|netcat -w 1 192.168.2.171 5555
#  echo ":SOUR2:BURS:MODE:TRIG;:SOUR2:BURS:TRIG:SOUR EXT;:SOUR2:BURS:NCYC 1 "|netcat -w 1 192.168.2.171 5555 #Set trigger to manual/external and number of bursts
#  sleep 0.2;
# the follwing line might not be needed
# echo ":SOUR2:BURS:TDEL 0.00${time[0]}"|netcat -w 1 192.168.2.171 5555 #Set time delay (in seconds)

}


function Arming()
{

echo "And output"
echo ":OUTP2 ON"|netcat -w 1 192.168.2.171 5555 #Turn on output of 

#========Frequency generator settings===========

    
}
   
   
   function PostDischargeAnalysis
{
    echo ":OUTP2 OFF"|netcat -w 1 192.168.2.171 5555 #Turn off output off ch2
      
}      
      
