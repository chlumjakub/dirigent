import sys
import h5py
import numpy as np
import matplotlib.pyplot as plt

def main():

    # Use python PlotSpectra.py "data/Spectrometer_halpha_1.h5"
    if len(sys.argv) < 2:
        print 'Filename not supplied, use: python PlotSpectra.py "data/Spectrometer_halpha_1.h5"'
        return

    filename = sys.argv[1]

    h5file=h5py.File(filename,"r")
   
    wl=h5file["Wavelengths"]
    sp=np.transpose(h5file["Spectra"])
    IntegrationTimeus=h5file["Spectra"].attrs["IntegrationTime_us"]
    
    
    fig, ax = plt.subplots(1, figsize=(8*1.5, 6*1.5)) # Create the figure and axes objects
    fig.suptitle('Integration Time = ' + str(IntegrationTimeus/1000.0) + ' ms')

    ax.set_xlabel( r'$\lambda$ (nm)')
    ax.set_ylabel( 'counts')
    ax.plot(wl,sp) # Plot the data
    
    plt.grid(b=True, which='major', color='#666666', linestyle='-') # Show the major grid lines with dark grey lines
    plt.minorticks_on() # Show the minor grid lines with very faint and almost transparent grey lines
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)

    plt.show()

if __name__ == "__main__":
    main()