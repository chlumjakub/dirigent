#/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh 

PC=Nb-Lenovo

notebook=RawDataGraph

HotFix=svoboda@Nb-Master

function Arming
{
    scp Setting.json golem@$PC:/home/golem/MiniSpectrometers/
    ssh golem@$PC "cd /home/golem/MiniSpectrometers;./SpectrometerControlApp.ex -i "Setting.json" -o "SettingOut.json" -s 0 -a > DumpedCommunication.txt" &
}

function RawDataAcquiring
{
    scp golem@$PC:/home/golem/MiniSpectrometers/Spectrometer_vis_0.h5 .
    scp golem@$PC:/home/golem/MiniSpectrometers/SettingOut.json .
    scp golem@$PC:/home/golem/MiniSpectrometers/DumpedCommunication.txt .
    cat /golem/shm_golem/shot_no > ShotNo
    
    scp $notebook.ipynb Spectrometer_vis_0.h5 Setting.json ShotNo $HotFix:Downloads/HotFix.Dg/
    ssh $HotFix "cd Downloads/HotFix.Dg; /home/svoboda/.local/bin/jupyter-nbconvert --execute $notebook.ipynb --to html --output $notebook.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)"
    scp $HotFix:Downloads/HotFix.Dg/ScreenShotAll.png .
    scp $HotFix:Downloads/HotFix.Dg/RawDataGraph.html .
    convert -resize $icon_size ScreenShotAll.png rawdata.jpg
    
}

function GraphZMB()
{

    jupyter-nbconvert --execute $notebook.ipynb --to html --output $notebook.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    convert -resize $icon_size ScreenShotAll.png rawdata.jpg
    
}

# Version SW trigger ssh golem@$PC "cd /home/golem/MiniSpectrometers;./SpectrometerControlApp.ex -i "Setting.json" -o "SettingOut.json" -v -s 0 -t sw -a > DumpedCommunication.txt"
