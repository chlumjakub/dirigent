#!/bin/bash
BasePath=../../..;source $BasePath/Commons.sh

# For Function definition see Devices/RelayBoards/Devices.sh


# Device service for the GOLEM operation
function GetReadyTheDischarge
{
    Call $RelayBoards Engage12V_24V_intosystem@Miscellaneous ON
}

function SecurePostDischargeState
{
    Call $RelayBoards Engage12V_24V_intosystem@Miscellaneous OFF
}


