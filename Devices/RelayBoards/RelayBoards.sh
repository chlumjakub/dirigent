BasePath=../..;source $BasePath/Commons.sh

function Relay {
RelayBoardName=$1
RelayNo=$2
SwitchTo=$3
    LogIt "#$RelayNo@$RelayBoardName  (${FUNCNAME[1]}) goes $SwitchTo .. "; 
    case "$SwitchTo" in
    'ON')  
    echo "*B1OS"$RelayNo"H"|telnet Quido-$RelayBoardName 10001  1>/dev/null 2>/dev/null;
    ;;
    'OFF')
    echo "*B1OS"$RelayNo"L"|telnet Quido-$RelayBoardName 10001 1>/dev/null 2>/dev/null;
    ;;
    esac
}

#echo "*B1OS5H"|telnet Quido-Racks 10001

# ============ South Sockets  ============
# 8-d .110 :5e


function ThirdFloor@SouthSockets  { Relay SouthSockets 7 $1; }
function Interferometry@SouthSockets  { ThirdFloor@SouthSockets "$@"; }
function Interferometry  { ThirdFloor@SouthSockets "$@"; }

function SecondFloor@SouthSockets  { Relay SouthSockets 6 $1; }

function FirstFloor@SouthSockets  { Relay SouthSockets 5 $1; }
function CurrentStabilization@SouthSockets  { FirstFloor@SouthSockets "$@"; }
function CurrentStabilization  { FirstFloor@SouthSockets "$@"; }


function GroundFloor@SouthSockets  { Relay SouthSockets ? $1; }

# ============ Tokamak sockets ============
# 8-e .111 :EE

#?? F30
#NorthWestDown F3
#EastUp F32
function SouthWestDown@TokamakSockets { Relay TokamakSockets 1 $1; }  #F34
function FastSpectrometry@TokamakSockets { Relay TokamakSockets 1 $1; } # F34
function Manipulator@TokamakSockets { Relay TokamakSockets 4 $1; } #NorthEastDown F33
#SouthEastDown F35
function SouthUp@TokamakSockets { Relay TokamakSockets 5 $1; } #F36 SouthUp
function FastCameras@TokamakSockets { Relay TokamakSockets 5 $1; } #F36 SouthUp
#NortWestUp F37

# ============ Infrastructure sockets ============
# 8-h .114 :8D

function XY@InfrastructureSockets { Relay InfrastructureSockets 3 $1; }
function 2NI_PC-VoSv@InfrastructureSockets { Relay InfrastructureSockets 3 $1; } 


# ============ North & Papouch Sockets  ============
# 8-f .112 :ef
function 2NI_PC-StepMal@NorthSockets_Papouch  { Relay NorthSockets_Papouch 2 $1; } #F32
function Pap-Kopecky@NorthSockets_Papouch  { Relay NorthSockets_Papouch 4 $1; }
function Pap-Zacek@NorthSockets_Papouch  { Relay NorthSockets_Papouch 5 $1; }
function Pap-Jakubek@NorthSockets_Papouch  { Relay NorthSockets_Papouch 6 $1; }
function Pap-Stockel@NorthSockets_Papouch  { Relay NorthSockets_Papouch 7 $1; }
function Pap-Jiranek@NorthSockets_Papouch  { Relay NorthSockets_Papouch 8 $1; }


# Miscellaneous 4-a .240 :4C
function Engage12V_24V_intosystem@Miscellaneous  { Relay Miscellaneous 1 $1; }
function Preionization-Heater@Miscellaneous  { Relay Miscellaneous 2 $1; }
function Preionization-Shortcut@Miscellaneous  { Relay Miscellaneous 3 $1; }

# PowerSupplies 8-a .248 :8D
function Kepcos4PositionStab@PowerSupplies  { Relay PowerSupplies 1 $1; }
function PowerSupplyHVcontactor@PowerSupplies  { Relay PowerSupplies 2 $1; }
function ShortCircuits@PowerSupplies  { Relay PowerSupplies 3 $1; }
function Baking@PowerSupplies  { Relay PowerSupplies 4 $1; }
function GlowDischPS@PowerSupplies  { Relay PowerSupplies 5 $1; }

#source Commons.sh;Call $RelayBoards Baking@PowerSupplies ON

# Permanent 8-b .254 :59
function Aten@Permanent  { Relay Permanent 4 $1; }
function Stop@Permanent  { Relay Permanent 7 $1; }
function TotalStop@Permanent  { Relay Permanent 8 $1; }

#  8-c .252 :
function SignalTowerRed@MiscII  { Relay MiscII 8 $1; }
function SignalTowerOrangeGreen@MiscII  { Relay MiscII 7 $1; }


# ============ Racks ============
# 8-g .113 :FA

function Miscellaneous@Racks { Relay Racks 1 $1; }
function Vacuum@Racks { Relay Racks 2 $1; }
function Bt@Racks { Relay Racks 3 $1; }
function Ecd@Racks { Relay Racks 4 $1; }
function TriggerStab@Racks { :; } #N/A

# Vacuum 16-a .241 :EC
function Majak@Vacuum { Relay Vacuum 1 $1; }
function GDmeter@Vacuum { Relay Vacuum 2 $1; }
function TMP-bStandby@Vacuum { Relay Vacuum 3 $1; }
function TMP-b@Vacuum { Relay Vacuum 4 $1; }
function MajorVent-b@Vacuum { Relay Vacuum 5 $1; }
function TMP-aStandby@Vacuum { Relay Vacuum 6 $1; }
function TMP-a@Vacuum { Relay Vacuum 7 $1; }
function MajorVent-a@Vacuum { Relay Vacuum 8 $1; }
function MinorVent-a@Vacuum { Relay Vacuum 9 $1; }
function MinorVent-b@Vacuum { Relay Vacuum 10 $1; }
function RotPump@Vacuum { Relay Vacuum 11 $1; }
function H2-pipe@Vacuum { Relay Vacuum 12 $1; }
function He-pipe@Vacuum { Relay Vacuum 13 $1; }

#source Commons.sh;Call $SHMS/Devices/RelayBoards RotPump@Vacuum ON




# Galvanics & PositionStabilization 16-b .233 :A0
# Galvanics
function SocketNo1@Galvanics { Relay Galvanics 1 $1; }
function TektrMSO64-a@Galvanics { SocketNo1@Galvanics "$@"; }
function TektrMSO58-a@Galvanics { Relay Galvanics 2 $1; }
function TektrMSO56-a@Galvanics { Relay Galvanics 3 $1; } #Basic diagnostics
function RigolMSO5204-c@Galvanics { Relay Galvanics 4 $1; } #Charger
function RigolMSO5204-d@Galvanics { Relay Galvanics 5 $1; } #Osciloscope on top of the tG
function GWInstekPSW-b@Galvanics { Relay Galvanics 6 $1; } #Preionization
function Radiometer@Galvanics { Relay Galvanics 7 $1; }
function Biasing@Galvanics { Relay Galvanics 8 $1; } 
#PositionStabilization
function MainSwitch@Stabilization { Relay PositionStabilization 12 $1; } 
function Kepcos@StabilizationON
{
for i in `seq 13 16`; do Relay PositionStabilization $i ON; sleep 2;done
}
function Kepcos@StabilizationOFF
{
for i in `seq 13 16`; do Relay PositionStabilization $i OFF; sleep 2;done
}
#Call $SHMS/Devices/RelayBoards Kepcos4PositionStab@PowerSupplies ON
#Call $RelayBoards Relay PositionStabilization 13 OFF

# MW preion 8-j .250

# Bt-Ecd 16-c .251 :7B
# Under GABO construction (horizon)
function XY@Bt-Ecd { Relay Bt-Ecd 1 $1; }

# LEDstrips 16-d .239 :35
# 

#Development issues
####################
#e.g. Call/DelayCall $SW/Devices/RelayBoards ShortCircuits@PowerSupplies OFF
#source Commons.sh;Call $SHMS/Devices/RelayBoards <<Copy/Paste>> ON/OFF
