#!/bin/bash

BasePath=../../../..;source $BasePath/Commons.sh


# For Function definition see Devices/RelayBoards/Devices.sh

function GetReadyTheDischarge()
{
    Call $RelayBoards  Majak@Vacuum ON
}

function SecurePostDischargeState()
{
    Call $RelayBoards  Majak@Vacuum OFF
}

