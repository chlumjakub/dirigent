"""Configuration to create a dashboard from a Jupyter notebook using the Bootstrap grid system

use as
.. code ::
    jupyter nbconvert  --config nbconvert_config.py  index.ipynb
"""


import os
from nbconvert.preprocessors import Preprocessor

c = get_config()

base_dir = os.path.dirname(__file__)

class GolemPreprocessor(Preprocessor):

    def preprocess(self, nb, resources):
        nb.cells = sorted(filter(lambda cell: 'golem' in cell['metadata'],
                                 nb.cells),
                          key=lambda cell: cell['metadata']['golem'].get('order', 0)
                          )
        # nb.cells = nb.cells[:2]
        return nb, resources

c.NbConvertApp.export_format = 'html'
c.TemplateExporter.preprocessors = [GolemPreprocessor]
c.TemplateExporter.exclude_input = True
c.TemplateExporter.exclude_output_prompt = True
c.TemplateExporter.template_file = os.path.join(base_dir, 'golem_dashboard.tpl')
