#!/bin/bash

BasePath=../../../..;
source $BasePath/Commons.sh #Dirigent
#source ../Universals.sh #This device
source $SW/Diagnostics/BasicDiagnostics4TrainCourses/commons/RemoteTrainingOsc.sh #Training


COMMAND="netcat -w 1 $ThisDev 5555"
scope_address="nc -w 1 $ThisDev 5555"

diags=("" "U_Loop" "U_BtCoil" "U_RogCoil" "U_photod")


function OpenSession()
{
    CommonOpenSession
    
}


function Arming()
{
	echo ":SINGLe"|$scope_address
}



function RawDataAcquiring()
{
    GetOscScreenShot 
    convert -resize $icon_size ScreenShotAll.png rawdata.jpg
    getdata

 
}

function GetOscScreenShot()
{
	echo ":SYSTem:KEY:PRESs MOFF"|$COMMAND
	mRelax
    #echo ":DISPLAY:DATA?  ON,OFF,PNG"|$COMMAND|tail -c +12 > $SHM0/$SUBDIR/$ThisDev/ScreenShot.png
    echo ":DISPLAY:SNAP? png"|netcat -w 4 $ThisDev 5555|tail -c +12 > ScreenShotAll.png
    #echo ":DISPLAY:DATA?  ON,OFF,PNG"|$COMMAND
}

function getdata()
{
    CommonGetData

}

