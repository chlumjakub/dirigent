#!/usr/bin/python2
# -*- coding: utf-8 -*-



from pygolem_lite import  Shot
from numpy import vstack
from pygolem_lite.modules import save_adv_par
import time, os, sys

def getdata():
    Data = Shot()
    CD_trigger = Data['tcd']
    #print CD_trigger
    #exit()

    from  isf_read import isf_read
    channel_matrix = []
    t = None
    for ch in ['ch1', 'ch2', 'ch3', 'ch4']:
        try:
            (data,tvec,header) = isf_read(ch+'.isf')
        except IOError:
            print 'file %s was not found'%(ch+'.isf')
            continue

	config = ""
	config += 'Time resolution:' + str(1/header.x_increment * 1e-6) + ' MHz\n'
	config += 'Bit depth:' + str(header.bit_num) + ' bit\n'
	config += "Range:" + str(0.5*header.y_multipt_const*2**header.bit_num) + header.y_unit + '\n'
	config += 'Setting:'+header.setting + '\n'

	f = open(ch + '.cfg', 'w')
	f.write(config)
	f.close()
	os.remove(ch+'.isf')

        t_offset = 0 # CD_trigger not used anymore for Tektronix
	t = tvec+t_offset
	channel_matrix.append(data)
        print  CD_trigger, t[0],t[-1]
        attr = [d for d in dir(header) if d[0]!= '_']
        for a in attr:
            print a, ':', getattr(header, a)

    print t
    #exit()
    save_adv_par('tektronix3014', t, vstack(channel_matrix).T)

    print "done"
    
	
def graphs(file_type):

    import matplotlib   
    matplotlib.rcParams['backend'] = 'Agg'
    matplotlib.rc('font',  size='10')
    matplotlib.rc('text', usetex=True)  # FIXME !! nicer but slower !!!

    name = "Tektronix"
    
    from pygolem_lite.modules import get_data, paralel_multiplot,saveconst,multiplot


    for i in range(1, 4+1):
        ch_name = 'ch%i' % i
        output_name = 'graph%i' % i
        out = get_data(ch_name, ch_name, 'U [V]', reduction = False)
        paralel_multiplot(out, name , output_name,  (9,3), 100, 'vertical', file_type )
        # integrated
        out = get_data(ch_name, ch_name, "U t [Vs]" , integrated=True )
        paralel_multiplot(out, name + ' Integrated' , output_name + '_intgr', (9,3), 100,  'vertical', file_type)

    
    #os.system("convert -resize 150x120\!  graph1.png icon.png")
    
    # icon
    #out = get_data('ch2', '', '', xlabel = "")
    #paralel_multiplot(out, "" , 'icon', (4,3), 40)
    saveconst('status', 0)


def main():
    try:
	if sys.argv[1] ==  "acquisition":
	    getdata()
	elif sys.argv[1] ==  "plots":
	    graphs('png')
    except IOError:
	print "Missing Tektronix data "
    

if __name__ == "__main__":
    main()
