#!/bin/bash

source /dev/shm/golem/Commons.sh
whoami="Devices/Oscilloscopes/TektrMSO64-a/LangRailProbesSweptWithFalco"

source Universals.sh


#Channels:
Nodiags=4
trigger=NULL
Nomaths=1
MericiOdpor=27

#source Commons.sh ;OpenSessionSomewhere /golem/Dirigent/Devices/Oscilloscopes/TektrMSO64-a/LangRailProbesSweptWithFalco.sh


function OpenSession()
{
echo ":ACQUIRE:MODE HIRes;
    :HORIZONTAL:MODE:MANUAL;
    :HORIZONTAL:MODE:MANUAL:CONFIGURE RECORDLENGTH;
    :HORIZONTAL:MODE MANUAL; 
    :HORIZONTAL:MODE:SAMPLERATE 2e6;
    :HORIZONTAL:MODE:SCALE 2.4e-3;
    :HORIZONTAL:POSITION 3 ;
    :SAVEon:TRIG ON;
    :SAVEON:WAVEform ON;
    :SAVEON:WAVEFORM:FILEFORMAT SPREADSheet;
    :SAVEon:FILE:DEST 'L:/';
    :SAVEON:IMAGE ON;
    :SAVEON:FILE:NAME 'TektrMSO64';
    :SAVEON:WAVEform:SOURCE ALL"|$COMMAND 1>/dev/null 2>/dev/null
    
    # for i in `seq 1 8`;do
    #    echo ":CH$i:SCALE 5;:CH$i:OFFSET 0;CH$i:LABEL:NAME 'ch$i';:DISplay:GLObal:CH$i:STATE ON"|$COMMAND  1>/dev/null 2>/dev/null
    #done

    echo "
    :CH1:SCALE 25;:CH1:POSITION -2;:CH1:OFFSET 0;:DISplay:GLObal:CH1:STATE ON;
    :CH2:SCALE 0.5;:CH2:OFFSET 0;:DISplay:GLObal:CH2:STATE ON;
    :CH3:SCALE 2;:CH3:POSITION -4;:CH3:OFFSET 0;:DISplay:GLObal:CH3:STATE ON;
    :CH4:SCALE 2;:CH4:POSITION -4;:CH4:OFFSET 0;:DISplay:GLObal:CH4:STATE ON;
    :CH1:LABel:NAME 'U_bias';
    :CH2:LABel:NAME 'U_res_LP';
    :CH3:LABel:NAME 'U_res_RP';
    :CH4:LABel:NAME 'U_loop-d'"|$COMMAND 1>/dev/null 2>/dev/null
    
    echo "FPANEL:PRESS SINGLESEQ;
    TRIGGER:A:MODE NORMAL;
    TRIGGER:A:TYPE EDGE ;
    TRIGGER:AUXL 1;
    TRIGGER:A:EDGE:SOURCE AUX"|$COMMAND 1>/dev/null 2>/dev/null
    
    echo "MATH:ADDNEW 'MATH1';
    MATH:MATH1:TYPE ADVANCED;
    MATH:MATH1:DEFine 'Ch2-Ch1';
    MATH:MATH1:LABel:NAMe 'U_probe_LP';
    MATH:MATH1:VUNIT 'V';
    :DISPLAY:WAVEVIEW1:MATH:MATH1:VERT:SCAL 20;
    :DISPLAY:WAVEVIEW1:MATH:MATH1:VERT:POS 2;
    "|$COMMAND 1>/dev/null 2>/dev/null
    
    echo "MATH:ADDNEW 'MATH2';
    MATH:MATH2:TYPE ADVANCED;
    MATH:MATH2:DEFine '-Ch2/$MericiOdpor';
    MATH:MATH2:LABel:NAMe 'I_probe_LP';
    MATH:MATH2:VERTICAL:SCALE 5;
    MATH:MATH2:HORIZONTAL:UNITS 'A';
    :DISPLAY:WAVEVIEW1:MATH:MATH2:VERT:SCAL 0.0002;
    :DISPLAY:WAVEVIEW1:MATH:MATH2:VERT:POS 1;
    "|$COMMAND 1>/dev/null 2>/dev/null
    #:DISPLAY:WAVEVIEW2:MATH:MATH2:AUTOSCALE 0;
    
    
    echo "PLOT:PLOT1:TYPe XY;:PLOT:PLOT1:SOUrce1 MATH1;:PLOT:PLOT1:SOUrce2 MATH2;:PLOT:PLOT1:LABEL:NAME 'Label';PLOT:ADDNEW 'PLOT1';"|$COMMAND 1>/dev/null 2>/dev/null
    
    ExternDataAvailabilityTest
    
    PrepareFilesToSHMs $SHMS Devices/`dirname $Drivers`

    
}


   
function Arming()
{

    LogTheDeviceAction

    rm -f /home/golem/tektronix_drop/TektrMSO64*.csv
    rm -f /home/golem/tektronix_drop/TektrMSO64*.png
    local u_fg=`cat $SHM0/Diagnostics/PetiProbe/Parameters/u_fg`;
      
    echo "
    :SAVEon:TRIG ON;
    :SAVEON:WAVEform ON;
    :SAVEON:WAVEFORM:FILEFORMAT SPREADSheet;
    :SAVEon:FILE:DEST 'L:/';
    :SAVEON:IMAGE ON;
    :SAVEON:FILE:NAME 'TektrMSO64';
    :DISplay:GLObal:CH1:STATE ON;
    :DISplay:GLObal:CH2:STATE ON;
    :DISplay:GLObal:CH3:STATE ON;
    :DISplay:GLObal:CH4:STATE ON;
    :SAVEON:WAVEform:SOURCE ALL"|$COMMAND 1>/dev/null 2>/dev/null
    
    #echo ":CH3:SCALE `echo 'scale=2;('$u_fg'*20)/5*47/1000'|bc`;:CH2:OFFSET 0;"|$COMMAND 1>/dev/null 2>/dev/null
    
	SingleSeq
}


function Web() 
{
    echo "<html><body>" > das.html
    WebRecDas "<h1>$ThisDev@GOLEM for Shot #$shot_no </h1>"
    WebRecDas "<a href="http://golem.fjfi.cvut.cz/shots/$shot_no/DASs/$ThisDev/">Data dir</a><br/>"
    WebRecDas "<a href="ScreenShotAll.png"><img src='ScreenShotAll.png' width='50%'></a><br/>"

 
}
   


function RawDataAcquiring()
{
    # for i in `seq 1 5`; do echo Call from Petiprobe $i;sleep 1; done
    getdata "1 2 3 4"


    #GetOscScreenShot
}
    

    
function getdata ()
{
local DataPath=/home/golem/tektronix_drop/


    timeout=10
    while [ ! -f $DataPath/TektrMSO64_ALL*.csv ];
    do
        if [ "$timeout" == 0 ]; then
        echo "ERROR: Timeout while waiting for the file from $whoami"
        exit 1
    fi
    sleep 1
    echo $timeout to wait for $whoami files
    ls -all $DataPath/TektrMSO64_ALL*.csv
    ((timeout--))
    done

    ls -all $DataPath/* > ls-all
    cp `ls  -d $DataPath/TektrMSO64_ALL_*.csv |tail -n 1` TektrMSO64_ALL.csv

    cp  `ls -d $DataPath/*|grep png|grep TektrMSO64` ScreenShotAll.png

    convert -morphology Dilate Octagon -resize 200x200 ScreenShotAll.png rawdata.jpg

}
    
    
    
