#!/bin/bash


COMMAND="netcat -q 1 $ThisDev.golem 4000"
Drivers="Oscilloscopes/Drivers/TektrMSO5/driver"

function ExternDataAvailabilityTest()
{
    
    $LogFunctionGoingThrough
    Arming
    Relax
    ForceTrig
    Relax
    #ls $Tek_mount_path
    if ! ls $Tek_mount_path/TektrMSO58_ch8*.csv > /dev/null 2>&1; then 
    critical_error "$ThisDev mount problem ... 192.168.2.116, tek_drop, golem, tokamak";fi
}




zmb()
{
    echo "
    :DISplay:GLObal:CH1:STATE ON;
    :DISplay:GLObal:CH2:STATE ON;
    :DISplay:GLObal:CH3:STATE ON;
    :DISplay:GLObal:CH4:STATE ON;
    :DISplay:GLObal:CH5:STATE ON;
    :DISplay:GLObal:CH6:STATE ON;
    :DISplay:GLObal:CH7:STATE OFF;
    :DISplay:GLObal:CH8:STATE ON"|$COMMAND 1>/dev/null 2>/dev/null
}

function SingleSeq()
{
    echo "FPANEL:PRESS SINGLESEQ"|$COMMAND 1>/dev/null 2>/dev/null
}   

function ForceTrig()
{
    echo "FPANEL:PRESS FORCETRIG"|$COMMAND 1>/dev/null 2>/dev/null
}

function Wake-upCall()
{
    ProtectElectricalGrid
    Call $SW/Infrastructure/Support/RelayBoards/KepcosGalvanics TektrMSO58-aON
    Call $SW/Infrastructure/Support/Sockets/Tokamak/ NortWestUpON #LYSO
}

function ShutDown()
{
    Sleep-downCall
}    

function Sleep-downCall()
{
    ProtectElectricalGrid
    Call $SW/Infrastructure/Support/RelayBoards/KepcosGalvanics TektrMSO58-aOFF
    Call $SW/Infrastructure/Support/Sockets/Tokamak/ NortWestUpOFF #LYSO
}

diags=('null' 'ch1' 'ch2' 'ch3' 'ch4' 'ch5' 'ch6' 'ch7' 'Trigger')

function getdata ()
{

local DataPath=/home/golem/tektronix_drop/
local DataRangeToAcq=$1

    timeout=40
    while [ ! -f /home/golem/tektronix_drop/TektrMSO58*.png ];
    do
        if [ "$timeout" == 0 ]; then
        echo "ERROR: Timeout while waiting for the file from Tek58"
        exit 1
    fi
    sleep 1
    echo $timeout s to wait for Tek58 files
    ls -all /home/golem/tektronix_drop/*.*
    ((timeout--))
    done
    
#    for i in `seq 1 5`;do echo "waiting for data save $i/9"; Relax;done
    LogIt "$ThisDev: Start of acquiring"

    ls -all $DataPath/* > ls-all
    
    for i in $DataRangeToAcq; do 
    tail -n +10 `ls -d /home/golem/tektronix_drop/*|grep TektrMSO58_ch$i` > ch$i.csv
    done
    cp `ls -d /home/golem/tektronix_drop/*|grep png|grep TektrMSO58` ScreenShotAll.png
    
    convert -morphology Dilate Octagon -resize 200x200 ScreenShotAll.png rawdata.jpg
    
    #Web 
    
    LogIt "$ThisDev: End of acquiring"
    
    
    
  
}


# echo "*IDN?"|netcat -w 1 TektrMSO58-a 4000
#echo "FPANEL:PRESS SINGLESEQ"|netcat -q 1 TektrMSO58-a 4000
# echo "FPANEL:PRESS FORCETRIG"|netcat -q 1 TektrMSO58-a 4000




# MOUNT: 192.168.2.116, tek_drop, golem, tokamak
# systemctl restart smbd
# Dat pozor, na ktery disk se mapuje .. O ci L

