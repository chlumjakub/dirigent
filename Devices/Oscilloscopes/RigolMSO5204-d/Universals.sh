#!/bin/bash

#BasePath=../../..;source $BasePath/Commons.sh

COMMAND="netcat -w 3 $ThisDev.golem 5555"
scope_address="nc -w 1 $ThisDev.golem 5555"



function Wake-upCall(){ 
DelayCall $RelayBoards RigolMSO5204-d@Galvanics ON
} 

function Sleep-downCall(){ 
DelayCall $RelayBoards RigolMSO5204-d@Galvanics OFF
}

function Arming()
{
	echo ":SINGLe"|$scope_address
}


function RawDataAcquiring()
{
local LastChannelToAcq=$1

    getdata $LastChannelToAcq
    GetOscScreenShot 
    #web
  
}

function GetOscScreenShot()
{
	echo ":SYSTem:KEY:PRESs MOFF"|$scope_address
	mRelax
    echo ":DISPLAY:DATA?  ON,OFF,BMP"|$COMMAND|tail -c +12 > ScreenShotAll.bmp
    convert ScreenShotAll.bmp ScreenShotAll.png
    convert -resize 200x200 ScreenShotAll.png rawdata.jpg
    rm ScreenShotAll.bmp 
}

function getdata()
{
local LastChannelToAcq=$1
#SETUP
    mkdir -p ScopeSetup;quer='XINC XOR XREF START STOP'; echo `for i in $quer; do echo :WAV:$i?";";done`'*IDN?'|$scope_address|sed 's/;/\n/g'|sed \$d > ScopeSetup/answers;for i in $quer; do echo _WAV_$i;done > ScopeSetup/querries;paste ScopeSetup/querries ScopeSetup/answers > ScopeSetup/ScopeSetup; 
    #cat ScopeSetup/ScopeSetup;
    awk '{print "echo " $2 " >ScopeSetup/"$1}' ScopeSetup/ScopeSetup |bash;

#TIME
    _WAV_XINC=`cat ScopeSetup/_WAV_XINC`;_WAV_START=`cat ScopeSetup/_WAV_START`;_WAV_STOP=`cat ScopeSetup/_WAV_STOP`; perl -e '$Time=0;for ( $i='$_WAV_START' ; $i <= '$_WAV_STOP'; $i++ ){printf "%.5e\n", $Time;$Time=$Time+'$_WAV_XINC'}' > Time
    
#DATA
    for CHANNEL in `seq 1 $LastChannelToAcq`; do 
	echo "$ThisDev oscilloscope:  Downloading data for channel $CHANNEL"
	echo ":WAV:SOURCE CHAN$CHANNEL;:WAV:FORM ASCii;:WAV:MODE NORM" |$scope_address;sleep 0.5s;echo ":WAV:DATA?" |$scope_address|cut -c 12-|sed 's/,/\n/g'>data
	paste -d, Time data > ${diags[$CHANNEL]}.csv
	gnuplot -e 'set datafile separator ",";set terminal jpeg;plot "'${diags[$CHANNEL]}.csv'" w l t "'${diags[$CHANNEL]/_/\\\\_}'"' > ${diags[$CHANNEL]}.jpg;
    done
    rm Time data

}



