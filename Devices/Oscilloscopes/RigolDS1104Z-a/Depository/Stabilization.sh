#!/bin/bash

ThisDev=RigolMSO5104-a.golem


scope_address="nc -q 3 $ThisDev 5555"


source ../../../Commons.sh


function OpenSession()
{
    echo "
CHANnel1:DISPlay ON;CHANnel1:PROBe 1;CHANnel1:SCALe 2;CHANnel1:OFFSet -6;
CHANnel2:DISPlay ON;CHANnel2:PROBe 1;CHANnel2:SCALe 0.5;CHANnel2:OFFSet -0.5;
CHANnel3:DISPlay ON;CHANnel3:PROBe 1;CHANnel3:SCALe 5;CHANnel3:OFFSet -10;
CHANnel4:DISPlay ON;CHANnel4:PROBe 1;CHANnel4:SCALe 0.5;CHANnel4:OFFSet 1;
TIMebase:DELay:ENABle OFF;TIMebase:MAIN:SCALe 2;TIMebase:MAIN:OFFSet 9;
:STOP;:CLEAR;
:SYSTem:KEY:PRESs MOFF
:TRIGger:EDGE:SOURce CHANnel1;TRIGger:EDGE:SLOPe POS;TRIGger:EDGE:LEVel 4;
"|$scope_address
}


function SingleSeq()
{
    echo ":SINGLe"|$scope_address
}  


function Arming()
{
	SingleSeq
}





function PostDischargeFinals()
{
    $LogFunctionGoingThrough
    getdata
 #   GetOscScreenShot

    echo OK

}

function getdata ()
{
    for channel in `seq 1 4`; do \
        echo "$ThisDev: Downloading data for channel $channel"
        echo ":WAV:SOURCE CHAN$channel;:WAV:FORM ASCii;:WAV:MODE NORM" |$scope_address;sleep 0.5s;echo ":WAV:DATA?" |$scope_address|cut -c 12-|sed 's/,/\n/g' > data$channel
    done

}
