

import numpy as np
import redpitaya_scpi as rp_scpi

from time import sleep


def time_to_index(time_mark, buff_freq=50):
    """Gets time in ms and returns a  number between
    0 and 16384 to indicate the index of the time.
    When we know the buffer will be used with
    buff_freq = 50 Hz, then it takes 20 ms to execute
    the buffer"""

    buff_duration = get_buff_duration(buff_freq=buff_freq)
    buff_length = 16384  # actual number of samples in buffer
    index = (time_mark/buff_duration)*buff_length

    index = int(index)

    return index


def get_buff_duration(buff_freq: int = 50): 
    """ Calculates the buffer duration from the
    SOUR:FREQ:FIX <frequency> that is give to the asg.

    Parameters
    ----------
    buff_freq : int
        Buffer frequency set in SOUR:FREQ:FIX <frequency>

    """
    buff_duration = (1/buff_freq)*1e3  # result in ms
    return buff_duration


def array_to_string(waveform: np.ndarray):
    """
    Takes a 1D np.ndarray and converts it into a string without
    enclosing brackets and with comma and space as a separator.

    :param np.ndarray waveform: 1D array
    :return: str waveform_str: properly formatted string representation
    of waveform suitable for SCPI ASG control
    """

    # creates a str representation of waveform, precision set to 5
    # because it is the way the RP team does that and floatmode to
    # fixed to always have 5 decimal places instead of " "
    # array_str_repr = np.array2string(waveform,
    #                                  precision=5,
    #                                  floatmode='fixed',
    #                                  threshold=np.inf)
    # # here the separating spaces are replaced with ", " i.e. comma and space
    # # and enclosing brackets are stripped off
    # waveform_string = array_str_repr.replace(" ", ", ")[1:-1]

    waveform_ch_10 = []
    for n in waveform:
        waveform_ch_10.append("{:.1f}".format(n))
    waveform_ch_1 = ", ".join(map(str, waveform_ch_10))

    return waveform_ch_1
    # return waveform_string


def roll_the_leds(rp_s):

    order = [0, 1, 2, 3, 7, 6, 5, 4]
    for led_num in order:
        rp_s.tx_txt('DIG:PIN LED{},1'.format(led_num))
        sleep(0.2)
        rp_s.tx_txt('DIG:PIN LED{},0'.format(led_num))
