
import redpitaya_scpi as rp_scpi
from time import sleep

rp_s = rp_scpi.scpi(address)  # creates a connection to the SCPI server



outp = 1
wave_form = "sine"
freq = 20
ampl = 3

# resets the generator to default
rp_s.tx_txt('GEN:RST')
# sets the arbitrary waveform
rp_s.tx_txt('SOUR{}:FUNC '.format(outp) + str(wave_form).upper())

# sets the output frequency
rp_s.tx_txt('SOUR{}:FREQ:FIX '.format(outp) + str(freq))

# sets the maximum amplitude
rp_s.tx_txt('SOUR{}:VOLT '.format(outp) + str(ampl))

# enables the burst mode
rp_s.tx_txt('SOUR{}:BURS:STAT BURST'.format(outp))
rp_s.tx_txt('SOUR{}:BURS:NCYC 1'.format(outp))

rp_s.tx_txt('OUTPUT{}:STATE ON'.format(outp))
rp_s.tx_txt('SOUR1:TRIG:SOUR EXT_PE'.format(outp))

# roll the leds
order = [0, 1, 2, 3, 7, 6, 5, 4]
for led_num in order:
    rp_s.tx_txt('DIG:PIN LED{},1'.format(led_num))
    sleep(0.2)
    rp_s.tx_txt('DIG:PIN LED{},0'.format(led_num))


# now there would be the trigger and then

rp_s.tx_txt('OUTPUT{}:STATE OFF'.format(outp))
