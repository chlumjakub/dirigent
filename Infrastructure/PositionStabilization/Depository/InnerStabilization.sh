BASEDIR="../.."
source $BASEDIR/Commons.sh



#NOTE Should be specified vaccum shot to work properly


function PostDischargeFinals 
{
    #export SHOT_NO=`cat ../../shot_no` #linux only
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" InnerStabilization.ipynb
    sed -i "s/vacuum_shot\ =\ 0/vacuum_shot\ =\ `cat /dev/shm/golem/ActualShot/Production/Parameters/vacuum_shot`/g" InnerStabilization.ipynb
    scp InnerStabilization.ipynb golem@Abacus:Stabilizace/ # Uprava4Abacus
    
    
    #jupyter-nbconvert  --ExecutePreprocessor.timeout=30 --execute notebook.ipynb --output analysis.html #Lokalni vypocet
    ssh golem@Abacus "cd Stabilizace;/home/golem/anaconda3/bin/jupyter  nbconvert --ExecutePreprocessor.timeout=300 --execute InnerStabilization.ipynb --output analysis.html"
    
    scp golem@Abacus:Stabilizace/analysis.html .
    scp golem@Abacus:Stabilizace/icon-fig.png .
    scp golem@Abacus:Stabilizace/video.mp4 .
    scp golem@Abacus:Stabilizace/plasma_position.csv .

    convert -resize $icon_size icon-fig.png graph.png
    convert -resize $icon_size icon-fig.png $BASEDIR/Diagnostics/LimiterMirnovCoils/analysis.jpg
    
    convert /$SHM/Management/imgs/MirnovCoils_icon.jpg -gravity center graph.png -gravity center +append icon_.png
    convert -bordercolor Black -border 2x2 icon_.png icon.png


}

