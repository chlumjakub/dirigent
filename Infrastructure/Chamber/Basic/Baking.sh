#!/bin/bash

function Baking_ON(){
DestinationTemp=$1
DestinationPressure=$2

#read -e -p "Mas odpojeny tyristor? [y/n]" -i 'n' answer; if [ $answer == "n" ]; then return;fi

    $LogFunctionStart;
     $psql_password;psql -c "INSERT into infrastructure.chamber (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('baking:start','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
     
    # DODELAT Call $SW/Devices/RelayBoards/ABBswitch-c/Et_commutator OFF
     sleep 2
    ActualChamberPressuremPa=`cat $SHML/ActualChamberPressuremPa`
    ActualChamberTemperature=`cat $SHML/ActualChamberTemperature`
    Call $RelayBoards Baking@PowerSupplies ON
    while (( $(bc <<< "$ActualChamberPressuremPa < $DestinationPressure") && $(bc <<< "$ActualChamberTemperature < $DestinationTemp") ));  do 
    for i in `seq 10`;do  read -r ActualChamberPressuremPa < $SHML/ActualChamberPressuremPa;if [ -z $ActualChamberPressuremPa ];then echo Problem;else break;fi; sleep 0.1;done;
        #read -r ActualChamberPressuremPa < $SHML/ActualChamberPressuremPa
    for i in `seq 10`;do  read -r ActualChamberTemperature < $SHML/ActualChamberTemperature;if [ -z $ActualChamberTemperature ];then echo Problem;else break;fi; sleep 0.1;done;
        #read -r ActualChamberTemperature < $SHML/ActualChamberTemperature
    echo `date "+%H:%M:%S"` : $ActualChamberPressuremPa/$DestinationPressure or $ActualChamberTemperature/$DestinationTemp
        sleep 1
    done
    Baking_OFF
    #Call $SW/Infrastructure/Support/PowerGrid/Baking DisEngage

    
}

function Baking_OFF(){

    $LogFunctionEnd;
    Call $RelayBoards Baking@PowerSupplies OFF
    $psql_password;psql -c "INSERT into infrastructure.chamber (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('baking:end','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
    #echo "UPDATE chamber_baking SET end_time='`date +%H:%M:%S`',end_pressure=`cat $SHML/ActualChamberPressuremPa`,end_temperature=`cat $SHML/ActualChamberTemperature`  WHERE id IN(SELECT max(id) FROM chamber_baking)"|ssh Dg "cat - |psql -q -U golem golem_database"
}

function Baking_test()
{
    $LogFunctionStart
    Baking_ON
    sleep 1s
    Baking_OFF
    $LogFunctionEnd

}


function ReadChamberTemp()
{
    PapagoReadCh1
}

