function GlowDischInitiate()
{
    $LogFunctionPassing;
    Call $SHMS/Devices/Vacuums/TMP-a StandbyON
    Call $SHMS/Devices/Vacuums/TMP-b StandbyON
    Call $SHMS/Devices/PowerSupplies/GWInstekGPR-a PowerON
    Call $SHMS/Devices/Vacuums/Valves-a CloseValves
    Call $RelayBoards He-pipe@Vacuum ON
    Call $RelayBoards Preionization-Shortcut@Miscellaneous ON #protect Am meter
    #Call $SHMS/Infrastructure/WorkingGas/Helium Engage
    Call $SHMS/Devices/Accessories/CurrentClampMeters/FlukeI30-a Engage
    $psql_password;psql -c "INSERT into infrastructure.chamber (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:init','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
    echo "!!ENGAGE HV PS & Open gas reservoir !!"
    echo './Dirigent.sh -ch gdwg_i # start WG_GD pressure ('$(<$SW/Infrastructure/Chamber/Basic/Parameters/U_GlowDisch_StartPressure)')'
    echo './Dirigent.sh -ch gdwg_o # operate WG_GD pressure ('$(<$SW/Infrastructure/Chamber/Basic/Parameters/U_GlowDisch_OperationPressure)')'
    echo './Dirigent.sh -ch gdw XX # wait XX minutes to stop GD'
    echo './Dirigent.sh -ch gds # Stop it'
    
    
}    


function GlowDischGasFlowSetup ()
{
    Voltage=$1
    Call $SHMS/Infrastructure/WorkingGas SetVoltage@GasValveTo $Voltage
    #Call Devices/PowerSupplies/GWInstekPSW-a GasFlowSetup $Voltage
    #Call $SHMS/Infrastructure/WorkingGas/Helium GasFlowSetup $Voltage
}    
    
    

function GlowDischWaitForStop()
{
Time=$1
    $psql_password;psql -c "INSERT into infrastructure.chamber (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:start','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
    echo "PROCES je @ golem@Dirigent>ps -Af|grep golem|grep GlowDischWaitForStop (awkkill pripadne)"
    echo " ============ ============ "
    for i in `seq 1 $Time`; do echo Waiting for GD to stop $i/$Time; sleep 1m;done
    GlowDischStop
    
}    

function GlowDischStop()
{
    $LogFunctionPassing;
    GlowDischGasFlowSetup 0
    sleep 10s
    Call $SHMS/Devices/PowerSupplies/GWInstekGPR-a PowerOFF
    Call $SHMS/Devices/Vacuums/TMP-a StandbyOFF
    Call $SHMS/Devices/Vacuums/TMP-b StandbyOFF
    sleep 5s
    Call $SHMS/Devices/Vacuums/Valves-a OpenValves
    Call $RelayBoards He-pipe@Vacuum OFF
    Call $RelayBoards Preionization-Shortcut@Miscellaneous OFF #protect Am meter

    #Call $SHMS/Devices/Accessories/CurrentClampMeters/FlukeI30-a DisEngage
    echo "!!Close He reservoir !!"
    $psql_password;psql -c "INSERT into infrastructure.chamber (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature) VALUES ('glow discharge:end','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`) " -q -U golem golem_database
}  
