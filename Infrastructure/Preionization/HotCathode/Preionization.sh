#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh


DeviceList="Interfaces/Gnome232-b/PreionizationPowSup"

Gnome232=Gnome232-b


function GetReadyTheDischarge()
{
    #LogTheDeviceAction 
    GeneralTableUpdateAtDischargeBeginning
    
    #powsup_heater=`cat $SHM0/Operation/Discharge/Preionization/Parameters/powsup_heater`
    powsup_heater=100
    num=$powsup_heater
    powsup_heater_format=$(printf "%03d\n" $powsup_heater)
    sum=0
    while [ $num -gt 0 ]
do
    mod=$(($num % 10))    #It will split each digits
    sum=$((sum + mod))   #Add each digit to sum
    num=$(($num / 10))    #divide num by 10.
done



#echo Sum@Preionization: $sum
chksum=$(printf "%x\n" $((10#$sum)))
#echo  CheckSum@Preionization:  $chksum

    VerboseMode "sum chksum"

echo -ne "@0ANAP"$powsup_heater_format"E"$chksum"\n"|telnet $Gnome232 10001 1>/dev/null 2>/dev/null
sleep 3
echo -ne "@0AOUT19A\n"|telnet $Gnome232 10001 1>/dev/null 2>/dev/null


Call $RelayBoards Preionization-Heater@Miscellaneous ON #PreionHeaterON
powsup_accel=$(<$SHM0/Infrastructure/Preionization/Parameters/u_accel)
#powsup_accel=100

echo "APPL $powsup_accel,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-b 2268

sleep 10
Call $RelayBoards Preionization-Shortcut@Miscellaneous ON #protect Am meter
}

function SecurePostDischargeState()
{
#echo "*B1OS1L"|telnet 192.168.2.240 10001 #PreionHeaterOFF
Call $RelayBoards Preionization-Heater@Miscellaneous OFF #PreionHeater OFF
mRelax
Call $RelayBoards Preionization-Shortcut@Miscellaneous OFF #protect Am meter
echo -ne "@0AOUT099\n"|telnet $Gnome232 10001  1>/dev/null 2>/dev/null
echo "APPL 0,2;OUTPut:IMMediate OFF"|netcat -w 1 GWInstekPSW-b 2268
}



