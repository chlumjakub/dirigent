#!/bin/bash

source Commons.sh

if [ -e $SHMS/session.setup ];  then 
    source $SHMS/session.setup
    for i in `ls /golem/shm_golem/Production/*`; do 
        issue=`basename $i`;
        #echo $issue
        declare $issue="`cat /golem/shm_golem/Production/$issue`";
        export $issue
    done;

#    Operation="Dirigent/HigherPower/Standard $Operation" # extra Dirigent Power
fi

if [ -e /dev/shm/golem/shot_no ]; then SHOT_NO=$(<$SHM/shot_no); fi




TASK=$1
#COMMANDLINE=`echo $@|sed 's/-r //g'`
# ToDo replace special chars: echo '!@#[]$%^&*()-' | sed 's/[]!@#[$%^&*()-]/\\&/g' ..... \!\@\#\[\]\$\%\^\&\*\(\)\-


case "$TASK" in
   "") 
      echo "Usage:
      *** Hotkeys:
      -h, -e                    :Help, Emergency
      -HpV  <<Voltage>>         :GasValveTo \$Voltage @H
      -wgc                      :WG default calibration
      -s w,p,os@d,r,sdc         :session issues: wake-up,ping,opensession@devices,reset everyth, sleep-down
      -d d,m,sws,s/swos,sujb/dds   :dummy,modest,standard w/o stab,sujb
      -t rr,git,b,mc            :rasps reboot,Go to ShotNo in thousands, backup, mount cams
      locals: Dgscps,Dgkillxterms
      -rs                       :rsync everything
      -i                        :Interupt everything
      -g                        :grep 
      -c                        :Call function@script in $SHMS, e.g. Dg -c Devices/Oscilloscopes/TektrDPO3014-a Wake-upCall
      -rel <<Ktere>> ON/OFF
      -dbrc                     :DELETE FROM remote.shots WHERE shots.status = 0 OR shots.status = 1
      -ch gd(i/g/s)/bon-def     :Glow discharge init->go, stop/baking"
      RETVAL=1
      ;;
      -e)
        Broadcast $SHMS "$Ensemble" SecurePostDischargeState
      ;; 
      -HpV)
      Voltage=$2
      Call $SHMS/Infrastructure/WorkingGas Engage
      Call $SHMS/Infrastructure/WorkingGas SetVoltage@GasValveTo $Voltage
      ;; 
      -i)
        touch $SHM0/Operation/Discharge/Parameters/Interupt
      ;;
      -c)
        Call $SHMS/$2 $3  
      ;;
      -rel)
        #Call $SHMS/Devices/RelayBoards  $2 $3  
        bash -c "cd $SW/Devices/RelayBoards;source RelayBoards.sh;$2 $3" 
      ;;
      -p)
        PingAllDevicesNtimes -N 1
      ;;
      -g)
      grep -r "$2" *|grep -v zmbs|grep -v html|grep -v Deposit|grep -v svg|grep -v ipynb|grep -v cpp|grep -v x3dom|grep -v Setups;
      ;;
      -wgc)
        xterm -fg yellow -bg blue -title "Golem WG calibration" -e "source Commons.sh;
        Call $SHMS/Infrastructure/WorkingGas H2Calibration 19 1 50 20"
        ;;
      -dbrc)
        $psql_password;psql -c "DELETE FROM remote.shots WHERE shots.status = 0 OR shots.status = 1" -q -U golem -d golem_database
      ;;
      -rs) #SW update everywhere
            source Commons.sh ; 
            rsyncRASPs 
            cd $SW
            cp Commons.sh Dirigent.sh $SHMS/;PrepareEnvironment@SHM $SHMS
            cp Commons.sh Dirigent.sh  $SHM0/;PrepareEnvironment@SHM $SHM0
            cp $SW/Devices/RelayBoards/RelayBoards.sh $SHMS/Devices/RelayBoards/
            cp $SW/Devices/Infrastructure/Racks/Universals.sh $SHMS/Devices/Infrastructure/Racks/
        ;;

      -h)
        echo "      ============ Session issues ============
      -s SetupSetup <<PathToSetup>>       :ln -s \$FromRootPathToSetup session.setup
      -s ping|p                 :PingAllDevices
      -s wake|w                 :Broadcast Devices Wake-upCall
      -s sleep|sdc              :Broadcast Devices Sleep-downCall
      -s open|o                 :Call Operation/Session/Basic SetupSession
      -s os@d                   :Broadcast Devices OpenSession
      -s reset|r                :Call Operation/Session/Basic ResetSession
      -s shutdown|s             :Broadcast Devices CloseSession
      ============ Discharge issues ============
      -d <<Parameters>>                   :make the discharge with all possible Parameters
      -d dummy|d              :bash Operation/Discharge/Basic/Styles/dummyCMD.html
      -d modes|m              :bash Operation/Discharge/Basic/Styles/modestCMD.html
      -d standard|s           :bash Operation/Discharge/Basic/Styles/standardCMD.html
      -d high|h               :bash Operation/Discharge/Basic/Styles/highCMD.html      
      -d vacuum|v             :bash Operation/Discharge/Basic/Styles/vacuumCMD.html      
      -d sujb|dds             :bash Operation/Discharge/Basic/Styles/sujbCMD.html      
      -d fullsteam|fs         :bash Operation/Discharge/Basic/Styles/fullsteamCMD.html
      ============ Chamber issues ============
      -ch pumping='on'|pon      :Call Chamber/Basic PumpingON
      -ch pumping='off'|poff    :Call Chamber/Basic PumpingOFF
      -ch baking='on'|bon       :Call Chamber/Basic Baking_ON <<TempLimit>> <<PressLimit>>, eg. Dg -ch bon 200 20
      -ch bon-def               :Call Chamber/Basic Baking_ON (200 5)
      -ch baking='off'|boff     :Call Chamber/Basic Baking_OFF
      -ch wgsv <<Voltage>>      :SetVoltage@GasValveTo \$Voltage @ChamberRASP
      -ch wgcal <<U_LowerLimit>> <<U_Step>> <<U_UpperLimit>> <<t_Relax>>, e.g. Dg --ch wgcal 16 0.25 22 25
      -ch wgcal-def             :(default) ~ Dg -ch wgcal 17 0.5 23 25
      -ch gdi                   :Call Chamber/Basic GlowDischInitiate
      -ch gdwg_i                :Call Chamber/Basic GlowDischGasFlowSetup START
      -ch gdwg_o                :Call Chamber/Basic GlowDischGasFlowSetup OPERATE
      -ch gdfs <<Voltage2Valve>>:Call Chamber/Basic GlowDischGasFlowSetup \$Voltage2Valve (68/45)
      -ch gds                   :Call Chamber/Basic GlowDischStop
      -ch gdw <<Time>>          :Call Chamber/Basic GlowDischWaitForStop \$Time
      ============ Retro actions ============
      -r WWWsReconstruction|wr <<ShotNo>>        : www for the \$ShotNo reconstruction;; e.g. Dg -r wr 41370
      -r PostDischargeAnalysis|pda <<ShotNo>> <<WHAT>>   : PostDischargeAnalysis for \$What@\$ShotNo; e.g. Dg -r pda 41370 Diagnostics/BasicDiagnostics/DetectPlasma/
      ============ Tools ============
      -t goto|g <<ShotNo>>        : cd /golem/database/operation/shots/\$ShotNo; e.g. Dg -t g 41338
      -t gotolastshot|gls        : cd /golem/database/operation/shots/<<LastShot>>
      -t gotointhousands|git <<XXShotNo>>        : cd /golem/database/operation/shots/((shot_no/1000*1000))+\$ShotNo; e.g. Dg -t git 338
      -t call|c <<FullPathToScript>> <<Function>>: e.g. Dg -t c /golem/database/operation/shots/41370/Diagnostics/BasicDiagnostics/DetectPlasma GetReadyTheDischarge
      -t rd                     :Run Remote deamon
      -t trigger|t1x or t100x                             :Make a trigger
      -t backup|b                              : Backup
      -t raspsreboot|rr         :Reboot rasps
      -t raspsync|rs            :Rsync rasps
      -t mfc                            :mount fast cameras
      --broadcast|-b opensession|os     :Broadcast Devices OpenSession
      --broadcast|-b emergency|e        :Broadcast Ensemble SecurePostDischargeState
      -t grep                           :Smart grep (-v zmbs html Deposit svg ipynb cpp js )
      -t cnp                            :Control panel (Ctrl-Supr-G)
      -t cmt <<What>>                   :Commit
      ============ Database issues ============
      -dbrc                     :DELETE FROM remote.shots WHERE shots.status = 0 OR shots.status = 1
      ============ @Dirigent NB ============
      Dgfgp                   :Reset feedgnuplot (run on local NB as alias)
      bash /golem/Dirigent/Operation/Session/Setup/Scopes.sh
      Dgkillxterms            :Sestreli vsechny xterminaly
      ssh4sql                 :Nahodi ssh reverse pro sql
      DgMons                  :cat Operation/Session/Setup/Monitors4GOLEM.sh"
      ;; 

      --tools|-t)
      case "$2" in 
        raspsreboot|rr)
            ssh golem@Chamber "sudo /sbin/reboot";
            ssh golem@Charger "sudo /sbin/reboot";
            ping Chamber
        ;;
        cmt)
            git add .; git commit -am "$3";git push #V Kate musi byt vse ulozeno/zavreno !
        ;;
        cnp)
            Call Infrastructure/Control/ControlPanel BasicManagementCore
        ;;
        mc)
            sudo mount.cifs //PhotronCamerasPC/FastCamGOLEM /mnt/share/PhotronCamerasPC/ -o user=golem
        ;;
        goto|g)
            cd /golem/database/operation/shots/$3;pwd
        ;;
        grep)
            grep -r "$3" *|grep -v zmbs|grep -v html|grep -v Deposit|grep -v svg|grep -v ipynb|grep -v cpp|grep -v js;     
        ;;
        mfc)
            sudo mount.cifs //PhotronCamerasPC/FastCamGOLEM /mnt/share/PhotronCamerasPC/ -o user=golem
        ;;
        gotointhousands|git)
            cd /golem/database/operation/shots/$(($(</golem/shots/0/shot_no)/1000*1000+$3));pwd
        ;;
        gotolastshot|gls)
            cd /golem/database/operation/shots/$(($(</golem/shots/0/shot_no)));pwd
        ;;
        call|c)
            Call $3 $4  
        ;;
        t1x) #basic trigger test
            Call $SHMS/Infrastructure/Triggering TriggerTest1x
        ;;
        t100x) #basic trigger test
            Call $SHMS/Infrastructure/Triggering TriggerTest100x
        ;;
        rd) 
            xterm -fg yellow -bg blue -title "Remote daemon" -e "bash Operation/Remote/RemoteInterface.script"
        ;;
        backup|b) #backup
            $psql_password;pg_dump golem_database > golem_database.sql; 
            rsync -r -u -v -K -e ssh --exclude '.git' $PWD svoboda@bn:backup/Dirigent/`date "+%y%m%d"`
            zip golem_database golem_database.sql;mpack -s "GM database `date`" golem_database.zip tokamakgolem@gmail.com;
            rm golem_database.*
      ;;
      esac
      return
      ;;
      --session|-s)
      case "$2" in 
        SetupSetup)
        cd $SW
        rm session.setup;ln -s $3 session.setup
        bash -c "cd $SW/Operation/Session/Basic;source Session.sh;SetupSession"
        ;;
      ping|p)
        PingAllDevicesNtimes -N 4
        #(for i in $(seq 1 4); do echo Ping $i/4; if PingAllDevices; then return; fi;done)
        #(PingAllDevices) #@Commons
        ;;
      ping_exit)
        (for i in $(seq 1 8); do echo Ping $i/8; if PingAllDevices; then exit; fi;done)
        ;;
      wake|w)
        (Broadcast $SHMS Devices Wake-upCall)
        ;;
      sleep|sdc)
        (Broadcast $SHMS Devices Sleep-downCall)
        ;;
      reset|r)
        bash -c "cd $SW/Operation/Session/Basic;source Session.sh;ResetSession"
        ;;
      open|o)
        Broadcast $SHMS Devices Wake-upCall
        (for i in $(seq 1 8); do echo Ping $i/8; if PingAllDevices; then exit; fi;done)
        for Dev in $RASPs;do Call $SHMS/Devices/ITs/`dirname $Dev` PrepareSessionEnv@SHM;done
        Broadcast $SHMS  "$Ensemble" OpenSession
        SubmitTokamakState "idle" 
        ;;
      os@d) 
        (Broadcast $SHMS Devices OpenSession)
      ;;

      shutdown|s)
      Call $SHMS/Operation/Session ShutDown
      ;;

      esac
      ;;
      --chamber|-ch)
      case "$2" in 
        pumping='on'|pon)
        xterm -fg yellow -bg blue -title "Golem pumping start" -e "source Commons.sh;Call $SHMS/Infrastructure/Chamber PumpingON"
        echo pumpingON
        ;;
        pumping='off'|poff)
        xterm -fg yellow -bg blue -title "Golem pumping end" -e "source Commons.sh;Call $SHMS/Infrastructure/Chamber PumpingOFF" 
        ;;
        baking-def='on'|bon-def) #baking ON with default values
        xterm -fg yellow -bg blue -title "Baking status" -hold -e "cd $SHMS/Infrastructure/Chamber; source Chamber.sh; Baking_ON 200 15" #Final Temperature Pressure
        ;;
        baking='on'|bon) #baking ON
        TempLimit=${3:-200};PressLimit=${4:-15};
        xterm -fg yellow -bg blue -title "Baking status" -hold -e "cd $SHMS/Infrastructure/Chamber; source Chamber.sh; Baking_ON $TempLimit $PressLimit" #Final Temperature Pressure
        ;;
        baking='off'|boff) #baking OFF
        cd $SHMS/Infrastructure/Chamber; source Chamber.sh; Baking_OFF; cd $SW
        ;;
        wgsv)
        Voltage=$3
        Call $SHMS/Infrastructure/WorkingGas/Hydrogen SetVoltage@GasValveTo $Voltage
        ;;
        wg0)
        ssh Chamber "source Rasp-WorkingGas.sh ;GasH2OFF;GasHeSetVoltage@GasValveTo $3";
        ;;
        wgcal) #With Lower $1 and Uppper $2 limits 
        # e.g. ./Dirigent.sh --wgcal 16 0.5 24 25
            LowerLimit=${3:-16.5};Step=${4:-0.25};UpperLimit=${5:-21};RelaxTime=${6:-15}
            xterm -fg yellow -bg blue -title "Golem WG calibration" -e "source Commons.sh;
            Call $SHMS/Infrastructure/WorkingGas/Hydrogen H2Calibration $LowerLimit $Step $UpperLimit $RelaxTime"
        ;;


        # ============ Glow discharge issues START ============
        gdi) #Glow discharge init
        Call $SHMS/Infrastructure/Chamber GlowDischInitiate
        ;;
        gdg) #Glow discharge go
        Call $SHMS/Infrastructure/Chamber GlowDischGasFlowSetup $(<$SW/Infrastructure/Chamber/Basic/Parameters/U_GlowDisch_StartPressure)
        read -e -p "GD nahozen, snizit tlak? [y/n]" -i 'y' answer; if [ $answer == "y" ]; then Call $SHMS/Infrastructure/Chamber GlowDischGasFlowSetup $(<$SW/Infrastructure/Chamber/Basic/Parameters/U_GlowDisch_OperationPressure) ;fi
        read -e -p "GD bezi, pockat 10 min? [y/n]" -i 'y' answer; if [ $answer == "y" ]; then Call $SHMS/Infrastructure/Chamber GlowDischWaitForStop 10 ;fi
        ;;
        gds) #Glow discharge stop 
        Call $SHMS/Infrastructure/Chamber GlowDischStop
        ;;
        gdwg_i) #Glow discharge init 
        Call $SHMS/Infrastructure/Chamber GlowDischGasFlowSetup $(<$SW/Infrastructure/Chamber/Basic/Parameters/U_GlowDisch_StartPressure)
        ;;
        gdwg_o) #Glow discharge operate
        Call $SHMS/Infrastructure/Chamber GlowDischGasFlowSetup $(<$SW/Infrastructure/Chamber/Basic/Parameters/U_GlowDisch_OperationPressure)
        ;;
        gdfs) 
        Voltage=$3
        #Call $SHMS/Infrastructure/Chamber GlowDischGasFlowSetup $Voltage
        Call $SHMS/Devices/PowerSupplies/GWInstekPSW-a GasFlowSetup $Voltage
        ;;
        gdw) 
        Time=$3 #[min]
        echo "PROCES je @ golem@Dirigent>ps -Af|grep golem|grep GlowDischWaitForStop (awkkill pripadne)"
        Call $SHMS/Infrastructure/Chamber GlowDischWaitForStop $Time
        ;;
      # ============ Glow discharge END ============
      esac
      ;;
      --retro|-r)
      case "$2" in
        WWWsReconstruction|wr)
        bash -c "
        cd /golem/database/operation/shots/$3/Infrastructure/Homepage;\
        zip `date '+%d%m%y-%H%M'`_WWWsReconstruction * -x *.zip;\
        cp -r $SW/Analysis/Homepage/Basic/* .;\
        source Homepage.sh; MakeProgressingPage Finalization";
        ;;
        PostDischargeAnalysis|pda)
        ShotNo=$3;What=$4
        bash -c "\
        cd /golem/database/operation/shots/$ShotNo/$What;\
        zip `date '+%d%m%y-%H%M'`_PostDischargeReconstruction * -x *.zip;\
        cp -r $SW/$What/* .;\
        source $(echo $What|xargs dirname|xargs basename).sh; PostDischargeAnalysis;
        ls -all;pwd";
        ;;
      esac
      ;;
      --broadcast|-b)
      case "$2" in
      opensession|os) 
        (Broadcast Devices OpenSession)
      ;;
      emergency|e)
        Broadcast "$Ensemble" SecurePostDischargeState
      ;; 
      esac
      ;;
      # ============ Tests START ============
      --discharge|-d)
      case "$2" in 
        style='dummy'|dummy|d)
        echo 'dummy' > $SHM/Production/style
        grep -v pre Operation/Discharge/Basic/Styles/dummyCMD.html|bash
        return
        ;;
        style='modest'|modest|m)
        pwd
        grep -v pre Operation/Discharge/Basic/Styles/modestCMD.html|bash
        return
        ;;
        style='standard-ws'|standard-with-stabilization|sws)
        grep -v pre Operation/Discharge/Basic/Styles/standard-with-position-stabilizationCMD.html|bash
        return
        ;;
        style='standard-wos'|standard-without-stabilization|swos|s)
        grep -v pre Operation/Discharge/Basic/Styles/standard-without-position-stabilizationCMD.html|bash
        return
        ;;
        style='sujb'|sujb|dds)
        grep -v pre Operation/Discharge/Basic/Styles/sujbCMD.html|bash
        return
        ;;

      esac   
        rm -rf  $SHM0 $SHMCLP/* 
        # Necessary Inicialization:
        mkdir -p $SHM0/Operation/Discharge $SHMCLP $SHM0TAGS
        cp Dirigent.sh Commons.sh $SHM0/ 
        cp $SW/Operation/Discharge/Basic/Discharge.sh $SHM0/Operation/Discharge/ #Sorry
        mv $SHM/Tags/* $SHM0TAGS/ 2>/dev/null
        cd $SHM0


        args=("$@")
        
        CL='./Dirigent.sh --discharge'
        #CL='./Dirigent.sh'

        while [ $# -gt 0 ]; do
        if [[ $1 == *"--"* ]]; then
            v="${1/--/}"
            if [ $v == "comment" ] || [[ "$v" =~ "diagnostics." ]] || [[ "$v" =~ "infrastructure." ]] || [ $v == "ScanDefinition" ] || [[ "$v" =~ "operation." ]]; then
                CL="$CL --$v \"$2\"";
                echo $2 > $SHMCLP/$v
            elif [ $v == "discharge" ]; then
                :
            else
                declare $v="$2"
                CL="$CL --$v $2"
                echo $2 > $SHMCLP/$v
            fi
        fi
    shift
    done
    echo "$CL" > $SHMCLP/CommandLine

    set -- $args
        Call $SHM0/Operation/Discharge Discharge
    ;;
          --dischange|--dc)
        rm -rf  $SHM0 
        # Necessary Inicialization:
        mkdir -p $SHM0/Operation/Discharge/ $SHMCLP $SHM0TAGS
        cp Dirigent.sh Commons.sh $SHM0/ 
        cp $SW/Operation/Discharge/Basic/Discharge.sh $SHM0/Operation/Discharge/ #Sorry  for Basic
        mv $SHM/Tags/* $SHM0TAGS/ 2>/dev/null

        args=("$@")
        
        CL='./Dirigent.sh --dischange'
        #CL='./Dirigent.sh'

        while [ $# -gt 0 ]; do
        if [[ $1 == *"--"* ]]; then
            v="${1/--/}"
            declare $v="$2"
            echo $2 > $SHMCLP/$v
    #       echo $v=$2
            if [ $v == "comment" ] || [[ "$v" =~ "diagnostics." ]] || [[ "$v" =~ "infrastructure." ]] || [ $v == "ScanDefinition" ] || [[ "$v" =~ "operation." ]]; then
                CL="$CL --$v \"$2\"";
            elif [ $v == "dischange" ]; then
            echo ;
            else
            CL="$CL --$v $2"
            fi
        fi
    shift
    done
    echo "$CL" > $SHMCLP/CommandLine

    set -- $args
        Call $SHMS/Operation/Discharge Discharge
    
#      ;;
esac


#Tuning ...
