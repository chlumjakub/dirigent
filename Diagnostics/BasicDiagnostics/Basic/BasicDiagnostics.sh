#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh

#diag_id=BasicDiagnostics/Basic
#setup_id="BasicDiagnostics"

DAS="Oscilloscopes/TektrMSO56-a/BasicDiagnostics"
DeviceList="$DAS"




# Scope file set-up: TIME,CH1,CH2,CH3,CH4,,TIME,MATH1,,TIME,MATH2
#columns="2 3 4 5 8 11" # Tek MSO64
columns="2 3 4 5 10 13" # Tek MSO56

diags=('U_Loop' 'U_BtCoil' 'U_RogCoil' 'U_LeybPhot' 'U_IntBtCoil' 'U_IntRogCoil')

notebook=StandardDAS.ipynb


function GetReadyTheDischarge ()
{
    
    GeneralTableUpdateAtDischargeBeginning
}


function PostDischargeAnalysis()
{
    #ls -all
    #GeneralDAScommunication $DAS RawDataAcquiring #TOHLE SE DELA v DETECT PLASMA!
    ln -s ../../Devices/`dirname $DAS` DAS_raw_data_dir
    #echo  ../../../Devices/$DAS DAS_raw_data_dir
    
    #n=0;for i in $columns; do tail -q -n +13  DAS_raw_data_dir/TektrMSO56_ALL.csv |awk -F "," '{print $1","'\$$i'}'> ${diags[$n]}.csv;((n+=1)); done;
  
    export SHOT_NO=`cat ../../shot_no` # for linux
    mkdir -p Results
    
    Analysis

    
    #GenerateDiagWWWs $diag_id $setup_id $DAS Diagnostics/Basic Sx48Muj5rcQZVGTLA
    GenerateDiagWWWs $instrument $setup $DAS $notebook
# @Commons.sh
    #GenerateWWWs
}

function Analysis
{
    
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" $notebook

    jupyter-nbconvert --execute $notebook \
                      --to html \
                      --output analysis.html \
                        > >(tee -a jup-nb_stdout.log) \
                       2> >(tee -a jup-nb_stderr.log >&2)

    convert -resize $icon_size icon-fig.png graph.png
}

# Tuning:

#cd "/golem/shm_golem/ActualShot/Diagnostics/BasicDiagnostics/"; cp /golem/svoboda/Dirigent/Diagnostics/BasicDiagnostics/StandardDAS.* .;source StandardDAS.sh ;PostDischargeAnalysis
