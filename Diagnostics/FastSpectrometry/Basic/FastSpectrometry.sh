#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh
DAS="DASs/cDAQ16-a/FastSpectrometry"
DeviceList="ITs/TimepixPC/FastSpectrometry $DAS"


columns="1 2" 
diags=('SpectrCh1 SpectrCh2')

function GetReadyTheDischarge ()
{
    GeneralTableUpdateAtDischargeBeginning
}


function Arming()
{
  ssh amos@TimepixPC -f "python3 nicDAQ/read_cdaq.py  --channels 0 1 --columns 'SiPhotodiode' 'FuncGen' -o nicDAQ/outfile.csv" >StandardLog 2>ErrorLog 
}


function PostDischargeAnalysis()
{
    scp amos@TimepixPC:nicDAQ/outfile.csv .
    ssh amos@TimepixPC "rm /home/amos/nicDAQ/outfile.csv"

    #Call $SHM0/Devices/$DAS RawDataAcquiring
    ln -s ../../../Devices/$DAS DAS_raw_data_dir
    
    n=0;for i in $columns; do cat nicDAQ/outfile.csv > ${diags[$n]}.csv;((n+=1)); done;
    
    GenerateDiagWWWs $instrument $setup $DAS

}


function RawDataAcquiring_()
{
    scp amos@TimepixPC:nicDAQ/outfile.csv .
    ssh amos@TimepixPC "rm /home/amos/nicDAQ/outfile.csv"
}

