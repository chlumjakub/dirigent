#!/bin/bash


function DefineDiagTable()
{
    CreateDiagnosticsTable $diag_id
    AddColumnToDiagnosticsTable $diag_id r_first_tip integer
    psql -c "COMMENT ON COLUMN  diagnostics.$diag_id.r_first_tip IS 'The distance between the first DRP tip and plasma center [mm]'" -q -U golem golem_database
    AddColumnToDiagnosticsTable $diag_id X_mod text
    psql -c "COMMENT ON COLUMN  diagnostics.$diag_id.X_mod IS 'I_sat (1) OR U_float (0) mode of the silver box '" -q -U golem golem_database
    AddColumnToDiagnosticsTable $diag_id I_SilverBox integer
    psql -c "COMMENT ON COLUMN  diagnostics.$diag_id.I_SilverBox IS 'The Silver box actual charging current'" -q -U golem golem_database
    
}
