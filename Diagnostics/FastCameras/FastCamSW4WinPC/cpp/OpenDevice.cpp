/** @file OpenDevice.cpp 
*   @brief Function used to search, detect and open cameras.
*/

#include "FastCamGOLEM.h"

void OpenDevices(vector<camera>& cams) {

    cout << "Device Search..." << endl;
    unsigned long nRet;
    unsigned long nErrorCode;

    unsigned long IPList[PDC_MAX_DEVICE];
    for (int i = 0; i < cams.size(); i++) { IPList[i] = cams[i].ipAddr; cout << "IPList[" << i << "]: " << IPList[i] << " = " << longToIP(IPList[i]) << endl; }
    

    PDC_DETECT_NUM_INFO DetectNumInfo;

    //------------ Camera(s) detection -----------
    nRet = PDC_DetectDevice(PDC_INTTYPE_G_ETHER,  // Gigabit-ethernet interface
        IPList,
        (unsigned long)cams.size(),                  // Max number of search devices
        PDC_DETECT_NORMAL,  // Indicate we're specifying an ip explicitly
        &DetectNumInfo,   // Output
        &nErrorCode);        // Output

    checkStatus("PDC_DetectDevice", nRet, nErrorCode);
    int detectedNum = DetectNumInfo.m_nDeviceNum;
    if (DetectNumInfo.m_nDeviceNum) {
        int detectedNum = DetectNumInfo.m_nDeviceNum;
        cout << "SDK reported " << detectedNum << " cameras found." << endl;
    }

    for (int k = 0; k < cams.size(); k++) {
        cams[k].isOpened = false; 
        for (int i = 0; i < detectedNum; i++) {
            if (cams[k].ipAddr == DetectNumInfo.m_DetectInfo[i].m_nTmpDeviceNo) {
                cams[k].detected = true;
                try {
                    nRet = PDC_OpenDevice(&(DetectNumInfo.m_DetectInfo[i]), &(cams[k].dev), &nErrorCode);
                }
                catch (...) { cout << "Try catch error" << endl; } /** @todo try, catch - maybe it is not necessary */
                checkStatus("PDC_OpenDevice", nRet, nErrorCode, true);
                cams[k].child = 1; // Hopefully it should always be 1
                cams[k].isOpened = true;
            }
       
        }
    }

    for (int k = 0; k < cams.size(); k++) {
        if (cams[k].isOpened == false) { cout << cams[k].name << " is not opened -> remove from cams" << endl; cams.erase(cams.begin() + k); }
    }
    
    
    //----------------- get some iformation from Camera(s) -----------------

    //get Device Code:
    for (int i = 0; i < cams.size(); i++) {
        nRet = PDC_GetDeviceCode(cams[i].dev, &(cams[i].deviceCode), &nErrorCode); 
        checkStatus("PDC_GetDeviceCode", nRet, nErrorCode, true);

        if (cams[i].deviceCode == PDC_DEVTYPE_FCAM_UX50) { cout << "cams[" << i << "].deviceCode: " << cams[i].deviceCode << " = FASTCAM MINI UX50" << endl; }

    }
    return;
}
