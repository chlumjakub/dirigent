/** @file getData.cpp
*	@brief Functions providing data storage in the desired format. 
*/
#include "FastCamGOLEM.h"


wstring convertCharArrayToLPCWSTR(const char* charArray) {
    const size_t WCHAR_ARRAY_LEN = 4096;
    std::unique_ptr<wchar_t[]> wcharArray(new wchar_t[WCHAR_ARRAY_LEN]);
    if (MultiByteToWideChar(CP_ACP, 0, charArray, -1, wcharArray.get(), WCHAR_ARRAY_LEN) == 0) {
        cout << "ERROR: Conversion of following string failed [" << charArray << "]."
            << " Returning an empty string." << endl;
        wcharArray[0] = 0;
    }
    return wcharArray.get();
}



void SetTransfer(vector<camera>& cams) { //it is necessary?
    unsigned long nRet, nErrorCode, n8BitSet, nBayer, nInterleave;

    for (int i = 0; i < cams.size(); i++) {
        nRet = PDC_GetTransferOption(cams[i].dev, cams[i].child, &n8BitSet, &nBayer, &nInterleave, &nErrorCode);
        checkStatus("PDC_GetTransferOption", nRet, nErrorCode);
        // set Bayer transfer mode
        nRet = PDC_SetTransferOption(cams[i].dev, cams[i].child, n8BitSet, PDC_FUNCTION_ON, nInterleave, &nErrorCode);
        checkStatus("PDC_SetTransferOption", nRet, nErrorCode);

    }
}


void SaveBMP(vector<camera>& cams) {
    unsigned long nRet, nErrorCode;
    unsigned long nFrameNo;
    PDC_FRAME_INFO frameInfo;

    string filenameStr;
    string nFrameNoStr;
    const char* filename;

    //Set Playback mode
    SetStatus(cams, PDC_STATUS_PLAYBACK);

    for (int i = 0; i < cams.size(); i++) {

        //Get FrameInfo
        nRet = PDC_GetMemFrameInfo(cams[i].dev, cams[i].child, &frameInfo, &nErrorCode);
        checkStatus("PDC_GetMemFrameInfo", nRet, nErrorCode);
        cout << "\nNumber of recored frames: " << frameInfo.m_nRecordedFrames << endl;

        for (nFrameNo = frameInfo.m_nStart; nFrameNo < cams[i].framesToSave; nFrameNo++) {

            nFrameNoStr = to_string(nFrameNo);
            filenameStr = cams[i].BMPdataFolderPath + nFrameNoStr + ".bmp";
            cout << filenameStr << endl;
            filename = filenameStr.c_str();

            //Save BMP files
            nRet = PDC_BMPFileSave(cams[i].dev, cams[i].child, convertCharArrayToLPCWSTR(filename).c_str(), nFrameNo, &nErrorCode);
            checkStatus("PDC_BMPFileSave", nRet, nErrorCode);
            //cout << "Writig frameNo: " << nFrameNo << endl;

        }
        cout << nFrameNo + 1 << " frames captured by  " << cams[i].name << " was saved to " << cams[i].BMPdataFolderPath << endl;

    }

}


void SaveAVI(vector<camera>& cams)
{
    unsigned long nRet, nErrorCode;
    unsigned long nFrameNo, AVIsize, AVIrate, nShowCompressDlg;
    PDC_FRAME_INFO frameInfo;

    nShowCompressDlg = 0; //Asi??
    AVIrate = 30;


    //Set Playback mode
    SetStatus(cams, PDC_STATUS_PLAYBACK);

    for (int i = 0; i < cams.size(); i++) {


        const char* filename = cams[i].videoFileName.c_str();

        //Get FrameInfo
        nRet = PDC_GetMemFrameInfo(cams[i].dev, cams[i].child, &frameInfo, &nErrorCode);
        checkStatus("PDC_GetMemFrameInfo", nRet, nErrorCode);
        cout << "\nNumber of recored frames: " << frameInfo.m_nRecordedFrames << endl;

        //Initialization to save AVI file
        nRet = PDC_AVIFileSaveOpen(cams[i].dev, cams[i].child, convertCharArrayToLPCWSTR(filename).c_str(), AVIrate, nShowCompressDlg, &nErrorCode);
        checkStatus("PDC_AVIFileSaveOpen", nRet, nErrorCode);

        for (nFrameNo = frameInfo.m_nStart; nFrameNo < cams[i].framesToSave; nFrameNo++) {
            //Save AVI files
            nRet = PDC_AVIFileSave(cams[i].dev, cams[i].child, nFrameNo, &AVIsize, &nErrorCode);
            checkStatus("PDC_AVIFileSave", nRet, nErrorCode);
            cout << "Writig frameNo: " << nFrameNo << endl;

        }
        cout << "Video recorded by  " << cams[i].name << " was saved to file " << filename << endl;
        //Termination to save AVI file
        nRet = PDC_AVIFileSaveClose(cams[i].dev, cams[i].child, &nErrorCode);
        checkStatus("PDC_AVIFileSaveClose", nRet, nErrorCode);

    }

}
