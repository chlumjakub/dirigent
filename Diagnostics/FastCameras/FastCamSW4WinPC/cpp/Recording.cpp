/** @file Recording.cpp 
*   @brief Functions used in recording part.
*/

#include "FastCamGOLEM.h"
#include <ctime>

void recording(vector<camera>& cams) {
	unsigned long nRet;
	unsigned long nErrorCode;
	unsigned long nStatus;
	unsigned long AFrames, RFrames, RCount, triggerType;


	for (int i = 0; i < cams.size(); i++) {
		//Trigger setting
		nRet = PDC_SetTriggerMode(cams[i].dev, 0, 0, 0, cams[i].TriggerMode.second, &nErrorCode);
		checkStatus("PDC_SetTriggerMode", nRet, nErrorCode);
		cout << "Trigger mode of " << cams[i].name << " was set to: " << cams[i].TriggerMode.first << endl;

		nRet = PDC_GetTriggerMode(cams[i].dev, &triggerType, &AFrames, &RFrames, &RCount, &nErrorCode);
		checkStatus("PDC_GetTriggerMode", nRet, nErrorCode);
		if (triggerType != cams[i].TriggerMode.second) { cout << "Trigger is set to " << triggerType << " instead of intended " << cams[i].TriggerMode.first << endl; }

		//Set a device to the recording ready status
		nRet = PDC_SetRecReady(cams[i].dev, &nErrorCode);
		checkStatus("PDC_SetRecReady", nRet, nErrorCode);

		////Set endless recording (TODO: Check if it is neccessary) - do not use it
		/*nRet = PDC_SetEndless(cams[i].dev, &nErrorCode);
		checkStatus("PDC_SetEndless", nRet, nErrorCode);*/


		//Confirms the operation mode of a device
		while (1) {
			nStatus = GetStatus(cams[i]);
			if (nStatus == PDC_STATUS_RECREADY) { break; }
		}


	}

	//Start Endless Recording
	cout << "Waiting for trigger" << endl;
	clock_t StartTime;

	for (int i = 0; i < cams.size(); i++) {
		if (cams[i].TriggerMode.second == PDC_TRIGGER_MANUAL) {
			cout << "Manual Trigger Mode." << endl;

			cout << "Press Enter to start recording..." << endl;
			(void)getchar(); //Explicitly ignore return value by (void)


			nRet = PDC_TriggerIn(cams[i].dev, &nErrorCode);
			StartTime = clock();
			checkStatus("PDC_TriggerIn", nRet, nErrorCode);
			cout << "Recording started." << endl;
		}
	}


	for (int i = 0; i < cams.size(); i++) { // TODO: toto je mozna trochu problem, neb bychom to potrebovali ve stejnem case pro obe kamery (stejne tak v predchozim)
		//Confirms the operation mode of a device
		while (1) {
			nStatus = GetStatus(cams[i]);
			if ((nStatus == PDC_STATUS_REC) && (nStatus != PDC_STATUS_RECREADY)) {
				cout << "loop for hardware trigger" << endl;
				StartTime = clock();
				break;
			}
		}
	}



	for (int i = 0; i < cams.size(); i++) { // TODO: toto je mozna trochu problem, neb bychom to potrebovali ve stejnem case pro obe kamery (stejne tak v predchozim)
		//Confirms the operation mode of a device
		while (1) {
			nStatus = GetStatus(cams[i]);
			if ((nStatus != PDC_STATUS_REC) && (nStatus != PDC_STATUS_RECREADY)) { cout << " loop for end " << endl; break; }
		}
	}


	//for (int i = 0; i < cams.size(); i++) { // TODO: toto je mozna trochu problem, neb bychom to potrebovali ve stejnem case pro obe kamery (stejne tak v predchozim)
	//	//Confirms the operation mode of a device
	//	while (1) {
	//		nStatus = GetStatus(cams[i]);
	//		if ((nStatus != PDC_STATUS_REC) && (nStatus == PDC_STATUS_RECREADY)) { break; }
	//	}
	//}

	clock_t EndTime;
	EndTime = clock();
	double duration = (EndTime - StartTime) / (double)CLOCKS_PER_SEC; //duration in sec
	cout << "End of the recording part \nDuration: " << duration << endl;
}
