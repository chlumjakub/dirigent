@mainpage FastCamGOLEM

The code was created in order to simplify work with cameras without the need to use the PFV4 program.

### External dependencies      
For proper functionality, all dll libraries must be in the same folder as the cpp files or the exe file (if it already exists).

See @subpage tutorials for usage examples.



**Warning:** 
The code is under permanent construction and not all features have been tested yet!

**Credit:**
This code was inspired by a program used on IPP Compass. 
