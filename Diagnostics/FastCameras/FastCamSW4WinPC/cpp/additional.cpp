/** @file additional.cpp
*	@brief Additional functions. 
*/
#include "FastCamGOLEM.h"

string translate_error(unsigned long nErrorCode) {
	string str;
	switch (nErrorCode) {
	case PDC_ERROR_NOERROR: str = "Normal (PDC_ERROR_NOERROR)."; break;
	case PDC_ERROR_UNINITIALIZE: str = "Uninitialize (PDC_ERROR_UNINITIALIZE)."; break;
	case PDC_ERROR_ILLEGAL_DEV_NO: str = "Illegal device number (PDC_ERROR_ILLEGAL_DEV_NO)."; break;
	case PDC_ERROR_ILLEGAL_CHILD_NO: str = "Illegal child number (PDC_ERROR_ILLEGAL_CHILD_NO)."; break;
	case PDC_ERROR_ILLEGAL_VALUE: str = "Illegal parameter (PDC_ERROR_ILLEGAL_VALUE)."; break;
	case PDC_ERROR_ALLOCATE_FAILED: str = "Memory allocate failed (PDC_ERROR_ALLOCATE_FAILED)."; break;
	case PDC_ERROR_INITIALIZED: str = "Initialized (PDC_ERROR_INITIALIZED)."; break;
	case PDC_ERROR_NO_DEVICE: str = "Device not found (PDC_ERROR_NO_DEVICE)."; break;
	case PDC_ERROR_TIMEOUT: str = "Time out (PDC_ERROR_TIMEOUT)."; break;
	case PDC_ERROR_FUNCTION_FAILED: str = "Function failed (PDC_ERROR_FUNCTION_FAILED)."; break;
	case PDC_ERROR_FUNCTION_DISABLE: str = "Function disabled (PDC_ERROR_FUNCTION_DISABLE)."; break;
	case PDC_ERROR_NO_DATA: str = "Recorded data does not exist (PDC_ERROR_NO_DATA)."; break;
	case PDC_ERROR_UNKNOWN_FRAME: str = "Unknown frame number (PDC_ERROR_UNKNOWN_FRAME)."; break;
	case PDC_ERROR_CAMERAMODE: str = "Camera mode error (PDC_ERROR_CAMERAMODE)."; break;
	case PDC_ERROR_NO_ENDLESS: str = "No endress mode recording (PDC_ERROR_NO_ENDLESS)."; break;
	case PDC_ERROR_FILEREAD_FAILED: str = "File read failed (PDC_ERROR_FILEREAD_FAILED)."; break;
	case PDC_ERROR_FILEWRITE_FAILED: str = "File write failed (PDC_ERROR_FILEWRITE_FAILED)."; break;
	case PDC_ERROR_IMAGE_SIZEOVER: str = "Image size error (PDC_ERROR_IMAGE_SIZEOVER)."; break;
	case PDC_ERROR_FRAME_AREAOVER: str = "Frame area error (PDC_ERROR_FRAME_AREAOVER)."; break;
	case PDC_ERROR_PLAYMODE: str = "Play mode error (PDC_ERROR_PLAYMODE)."; break;
	case PDC_ERROR_NOT_SUPPORTED: str = "Not supported (PDC_ERROR_NOT_SUPPORTED)."; break;
	case PDC_ERROR_DROP_FRAME: str = "Frame is not captured (PDC_ERROR_DROP_FRAME)."; break;
	case PDC_ERROR_FILE_OPEN_ALREADY: str = "File has been already opened (PDC_ERROR_FILE_OPEN_ALREADY)."; break;
	case PDC_ERROR_FILE_NOTOPEN: str = "File is not opened (PDC_ERROR_FILE_NOTOPEN)."; break;
	case PDC_ERROR_CONVERSION_OF_STRING: str = "Conversion of string. Multi-byte and Unicode (PDC_ERROR_CONVERSION_OF_STRING)."; break;
	case PDC_ERROR_LOAD_FAILED: str = "Module load failed (PDC_ERROR_LOAD_FAILED)."; break;
	case PDC_ERROR_FREE_FAILED: str = "Module free failed (PDC_ERROR_FREE_FAILED)."; break;
	case PDC_ERROR_LOADED: str = "Module loaded (PDC_ERROR_LOADED)."; break;
	case PDC_ERROR_NOTLOADED: str = "Module is not loaded (PDC_ERROR_NOTLOADED)."; break;
	case PDC_ERROR_UNDETECTED: str = "Device not yet detect (PDC_ERROR_UNDETECTED)."; break;
	case PDC_ERROR_OVER_DEVICE: str = "Device kind is too multi (PDC_ERROR_OVER_DEVICE)."; break;
	case PDC_ERROR_INIT_FAILED: str = "Initialize failed (PDC_ERROR_INIT_FAILED)."; break;
	case PDC_ERROR_OPEN_ALREADY: str = "Device is opened (PDC_ERROR_OPEN_ALREADY)."; break;
	case PDC_ERROR_NOTOPEN: str = "Device is not opened (PDC_ERROR_NOTOPEN)."; break;
	case PDC_ERROR_LIVEONLY: str = "Not become LIVE mode (PDC_ERROR_LIVEONLY)."; break;
	case PDC_ERROR_PLAYBACKONLY: str = "Not become PLAYBACK mode (PDC_ERROR_PLAYBACKONLY)."; break;
	case PDC_ERROR_SEND_ERROR: str = "Send error (PDC_ERROR_SEND_ERROR)."; break;
	case PDC_ERROR_RECEIVE_ERROR: str = "Receive error (PDC_ERROR_RECEIVE_ERROR)."; break;
	case PDC_ERROR_CLEAR_ERROR: str = "Initialize error (PDC_ERROR_CLEAR_ERROR)."; break;
	case PDC_ERROR_COMMAND_ERROR: str = "Command error (PDC_ERROR_COMMAND_ERROR)."; break;
	case PDC_ERROR_COMPARE_DATA_ERROR: str = "Compare data error (PDC_ERROR_COMPARE_DATA_ERROR)."; break;
	}
	return str;
}

void checkStatus(string functionName, unsigned long nRet, unsigned long nErrorCode, bool addException) {
	if (nRet == PDC_FAILED) {
		cout << "Function " << functionName << " failed with error code " << nErrorCode << ": " << translate_error(nErrorCode) << endl;
		if (!addException) { 
			throw "Throwing an error";
			//cout << "Program termination...." << endl; 
			//exit(0); 
		}
	}
	return;
}

int getClosestValue(int DesiredValue, unsigned long* List) {
	bool correctValue;
	correctValue = false;
	for (int i = 0; i <= PDC_MAX_LIST_NUMBER; i++) {
		if (List[i] == DesiredValue) {
			correctValue = true;
			cout << "Desired value:" << DesiredValue << " is OK" << endl;
			break;
		}
	}
	if (!correctValue) {
		cout << "Value can not be set -> find closest value" << endl;
		if (DesiredValue < List[0] || DesiredValue > List[PDC_MAX_LIST_NUMBER - 1]) {
			cout << "DesiredValue: " << DesiredValue << " is out of range [" << List[0] << "," << List[PDC_MAX_LIST_NUMBER - 1] << "]" << endl;
		}

		else {
			int i = 1;
			cout << "Desired Value " << DesiredValue << endl;
			while (DesiredValue > List[i]) { i++; }
			
			if (abs(long(List[i] - DesiredValue)) < abs(long(DesiredValue - List[i - 1]))) { DesiredValue = List[i]; }
			else { DesiredValue = List[i - 1]; }
			cout << "The value was set to " << DesiredValue << endl;
		}
	}
	return DesiredValue;
}

unsigned long find_closest_setting(unsigned long val, unsigned long* list, unsigned long n, unsigned long side) {
	unsigned long i;
	for (i = 0; i < n; i++) {
		if (val <= list[i]) break;
	}
	i = i + side;
	if (i > n) i = n;
	if (i < 0) i = 0;
	return list[i];
}

unsigned long find_closest_value(unsigned long val, unsigned long* list, unsigned long n) {
	unsigned long i;
	long delta;
	int index_bsf = 0;
	long delta_bsf = LONG_MAX;
	for (i = 0; i < n; i++) {
		delta = abs((long)val - (long)list[i]);
		if (delta < delta_bsf) {
			index_bsf = i;
			delta_bsf = delta;
		}
	}
	return list[index_bsf];
}


string longToIP(unsigned long num) {
	stringstream ip;
	for (int i = 3; i >= 0; i--) { ip << std::setfill('0') << setw(3) << ((num >> (8 * i)) & 0xff) << ((i > 0) ? "." : ""); }
	return ip.str();
}

unsigned long IPtoLong(string str) {
	if (str.find_first_not_of("0123456789.") != string::npos) { cout << "Wrong character in IP, returning zero" << endl; return 0; }
	unsigned long n, l = 0;
	size_t pos = 0;
	for (int i = 3; i >= 0; i--) {
		n = stoi(str.substr(pos, 3));
		l = l + (n << (8 * i));
		pos = str.find('.', pos) + 1;
		if (((pos == string::npos + 1) && (i > 0)) || (n > 255)) { cout << "Wrong IP format, returning zero" << endl; return 0; }
	}
	return l;
}

void printCamsPar(vector<camera>& cams) {
	for (int i = 0; i < cams.size(); i++) {
		cout << cams[i].name << endl;
		cout << "\tIP address : " << longToIP(cams[i].ipAddr) << endl;
		//cout << "\tinput 1: " << cams[i].input1.first << endl;
		//cout << "\tinput 2: " << cams[i].input2.first << endl;

		cout << "\tWidth: " << cams[i].width << endl;
		cout << "\tHeight: " << cams[i].height << endl;
		cout << "\tRecord rate: " << cams[i].recordRate << endl;

		cout << "\tTrigger mode: " << cams[i].TriggerMode.first << ": " << cams[i].TriggerMode.second << endl;
		cout << "\tFrames to save: " << cams[i].framesToSave << endl;
		cout << "\tDuration of video (real time => in ms): " << cams[i].shotDurToSave << endl;
		cout << "\tAVI file name: " << cams[i].videoFileName << endl;
	}
}

void checkNumFrameToSave(vector<camera>& cams) {

	cout << "inside FrameToSAve " << endl;
	
	for (int i = 0; i < cams.size(); i++) {
		cout << cams[i].recordRate * cams[i].shotDurToSave / 1000 << endl;
		if (cams[i].recordRate * cams[i].shotDurToSave / 1000 > cams[i].framesToSave) {
			cams[i].framesToSave = cams[i].recordRate * cams[i].shotDurToSave/1000;
			cout << "Amout of frames that will be save was changed to " << cams[i].framesToSave << endl;
		}
	}

};