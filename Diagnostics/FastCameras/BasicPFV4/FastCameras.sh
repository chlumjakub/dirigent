BasePath=../../..;source $BasePath/Commons.sh

DeviceList="ITs/PhotronCamerasPC/FastCameras AVs/PhotronMiniUX50-a/Radial AVs/PhotronMiniUX50-b/Vertical"





function Wake-upCall(){ 
    Call $SW/Infrastructure/PowerGrid/Tokamak FastCamerasON
 } 

function Sleep-downCall(){ 
    Call $SW/Infrastructure/PowerGrid/Tokamak FastCamerasOFF
}


function PostDischargeAnalysis
{
    #convert -resize $icon_size SpeedCamera.png analysis.jpg
    sleep 25
    mkdir Camera_Radial
    mkdir Camera_Vertical
    cp /mnt/share/`ls -1tr /mnt/share/|grep C002|tail -1`/*.* Camera_Radial/
    cp /mnt/share/`ls -1tr /mnt/share/|grep C001|tail -1`/*.* Camera_Vertical/
    
    sed -i "s/shot_no=0/shot_no\ =\ `cat $SHMS/shot_no`/g" Camera_Position.ipynb 
    jupyter-nbconvert --ExecutePreprocessor.timeout=60 --to html --execute Camera_Position.ipynb --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    convert -resize $icon_size icon-fig.png graph.png
    
    
    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` Diagnostics/FastCameras # @Commons.sh
}

function SetupCams()
{
echo '[
  {
    "BMPdataFolderPath": "BMP\\Vertical\\",
    "TriggerMode": "TrigStart",
    "framesToSave": 1200,
    "height": 58,
    "input1": "TrigPos",
    "input2": "SyncPos",
    "ipAddr": "192.168.2.10",
    "name": "Camera Vertical",
    "recordRate": 40000,
    "shotDurToSave": 30,
    "videoFileName": "AVI\\CamVert.avi",
    "width": 1280
  },
  {
    "BMPdataFolderPath": "BMP\\Radial\\",
    "TriggerMode": "TrigStart",
    "framesToSave": 1200,
    "height": 56,
    "input1": "TrigPos",
    "input2": "SyncPos",
    "ipAddr": "192.168.2.11",
    "name": "Camera Radial",
    "recordRate": 40000,
    "shotDurToSave": 30,
    "videoFileName": "AVI\\CamRad.avi",
    "width": 1280
  }
]' > CamsConfig.json
}
