#!/usr/bin/python2
# -*- coding: utf-8 -*-

import matplotlib 
matplotlib.rcParams['backend'] = 'Agg'
matplotlib.rc('font',  size='10')
matplotlib.rc('text', usetex=True)  # FIXME !! nicer but slower !!!

#import pygolem_lite


from numpy import *
#from pygolem_lite.config import *
from pygolem_lite.modules import *
#from matplotlib.pyplot import *
import time
from shutil import copy
import os, sys

# information about cards as tuples (data_file_name, graph_label, data_key)
cards = [ ("Nidatap_6133", "NI Standard 6133", "nistandard"),
          #("Nidatap_6132", "NI Standard 6132", "nistandard6132"),
          ]

def graphs(file_type):
    for fname, gname, key in cards:  # iterate over filenames and graph labels
        for integrated in [False, True]: # draw both integrated and plain
	    try:
		out = get_data(key, 'ch', '',xlim=[0,40], integrated=integrated, reduction=True)
		if out is None:
		    continue
		g_label = gname     # graph label
		g_fname = "graph_" + fname # file name to save graph to
		if integrated:
		    g_label += " Integrated" 
		    g_fname += "_int" 
		multiplot(out, g_label , g_fname, (6,len(out)*2), 100,  'vertical', file_type)
		time.sleep(1)  #why??
		os.system('convert -resize 150x120\! graph_%s.png icon.png' % cards[0][0]) 
	    except Exception, e:
		print "Failed plotting:  " + g_label + " "  + str(e) 

def acquisition():
    for fname, _, _ in cards: # graph name and key discarded
	try:
	    load_adv(fname) #tuple unpacking of (tvec, data)
	except Exception, e:
	    print "Failed saving: " + str(e) 

def main():
    if sys.argv[1] ==  "plots":
	graphs('png')
    if sys.argv[1] ==  "acquisition":
	acquisition()
	saveconst('status', 0)



if __name__ == "__main__":
    main()
