#!/bin/bash

DAS="DASs/Papouch-Za/DoubleRakeProbe"
# shellcheck source=/dev/shm/golem/Commons.sh
BasePath=../..;source $BasePath/Commons.sh


diag_id=DoubleRakeProbe

#DeviceList="RelayBoards/Energenie_LANpower-a/PowerSupply4DASs $DAS"
DeviceList="$DAS Tools/SilverBox-b/DoubleRakeProbe"


LastChannelToAcq=12
#LastChannelToAcq=2
# (1,2,3,4,5,6,10,11,12,13,14) Oznaceni kabelů z DRP
diags=('DRP-R1' 'DRP-R2' 'DRP-R3' 'DRP-R4' 'DRP-R5' 'DRP-R6'  'DRP-L1' 'DRP-L2' 'DRP-L3' 'DRP-L4' 'DRP-L5' 'DRP-L6')


function GetReadyTheDischarge
{
    GeneralTableUpdateAtDischargeBeginning
}


function PostDischargeAnalysis() 
{

    #GeneralDAScommunication $DAS RawDataAcquiring $LastChannelToAcq
    ln -s $BasePath/Devices/`dirname $DAS` DAS_raw_data_dir

    for i in `seq 1 $LastChannelToAcq` ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-1]}.csv
    done        

    Analysis
    GenerateDiagWWWs $instrument $setup $DAS 
   
}

function NoAnalysis
{
    cp $SW/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png

}


function Analysis
{
    local whoami="FilPap"
    # finds 'shot_no = 0' and replaces the 0 with the current shot number
    # in the specified jupyter notebook
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ $(cat $SHMS/shot_no)/g" $whoami.ipynb

    # creates analysis.html from the given .ipynb file
    # appends stdout to jup-nb_stdout.log and print it in console
    # appends stderr to jup-nb_stderr.log and print it in console
    
    IfNoPlasmaThenReturn
    
    jupyter-nbconvert --ExecutePreprocessor.timeout=30 \
                      --to html \
                      --execute $whoami.ipynb \
                      --output analysis.html \
                        > >(tee -a jup-nb_stdout.log) \
                       2> >(tee -a jup-nb_stderr.log >&2) #Lokalni vypocet

    convert -resize $icon_size icon-fig.png graph.png
}


