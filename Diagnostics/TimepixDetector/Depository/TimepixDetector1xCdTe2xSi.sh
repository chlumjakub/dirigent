#!/bin/bash

SHM="/dev/shm/golem"
source $SHM/Commons.sh
#rPi=192.168.2.92 ## rPI IP adress 
shot_no=`CurrentShotDataBaseQuerry shot_no`

diag_id="TimepixDetector"
setup_id="TimepixDetector1xCdTe2xSi"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Computers/RasPi4-d/TimepixDetector Computers/RasPi4-b/TimepixDetector  Computers/RasPi4-e/TimepixDetector"
DeviceList="$DAS"
Rasps="TimePix-Si-a TimePix-Si-b TimePix-CdTe-a"
JpNbs="TimePix-Si-a TimePix-Si-b"

function Arming()
{
    for rPi in $Rasps; do 
        echo "Arming $rPi"
        ssh pi@$rPi "rm data/*"
        ssh pi@$rPi "python3 arming.py $shot_no > /dev/null 2>/dev/null &"
	done
	#ssh pi@$rPi "python3 test2.py $shot_no  &"
	#ssh pi@$rPi "python3 test2.py $shot_no"
}




function PostDischargeAnalysis()
{
      sleep 5
        for rPi in $Rasps; do 
        echo "PostDischargeAnalysis @ $rPi"
              mkdir -p $rPi
              scp pi@$rPi:data/* $rPi/
        done

#local nb_id="TimePix-c/t3pa_2cls_tokamak"

   #export SHOT_NO=`cat ../../shot_no` #linux only
   
   for nb_id in $JpNbs; do 
     sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat $SHMS/shot_no`/g" $nb_id/t3pa_2cls_tokamak.ipynb
     jupyter-nbconvert  --ExecutePreprocessor.timeout=20 --to html --execute $nb_id/t3pa_2cls_tokamak.ipynb --output analysis.html         > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
   done   
 
        
    cp TimePix-b/icon-fig.png TimePix-b/analysis.html .    
    convert -resize $icon_size icon-fig.png graph.png
        
       GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` $GooglePhotosPath # @Commons.sh

}

