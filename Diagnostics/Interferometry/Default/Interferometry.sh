#!/bin/bash

 
BasePath=../..;source $BasePath/Commons.sh

DAS="Oscilloscopes/TektrDPO3014-a/Interferometry"

DeviceList=$DAS






 

function GetReadyTheDischargeZMB ()
{

    #GeneralTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
    GeneralTableUpdateAtDischargeBeginning diagnostics.interferometry #@Commons.sh
}



# function PostDischargeAnalysis() is @Commons


function Analysis
{
local nb_id="phase_detection_density"


    IfNoPlasmaThenReturn
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat $SHMS/shot_no`/g" $nb_id.ipynb
    jupyter-nbconvert --execute $nb_id.ipynb --to html --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    

    convert -resize $icon_size icon-fig.png graph.png
}

function NoAnalysis
{

#Workarround
    echo "<HTML><HEAD><TITLE></TITLE><BODY>
    <H1>Diagnostics analysis is not ready yet</H1>
    <!-- <img src="icon-fig.png"></img>-->
    <br></br></BODY></HTML>" > analysis.html
    cp $SW/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
#Finals      
    convert -resize $icon_size icon-fig.png graph.png
}


