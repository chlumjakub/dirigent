ShotNo=0;

adress = ['http://golem.fjfi.cvut.cz/shots/' num2str(ShotNo) '/Devices/DASs/Papouch-Ji/Radiometer/'];
rad_ch = [ 1 2 3 4 5 6 7 8 10 11 13] ;

%% Callibration coefficients
callibration = [0.297012463760678 -0.875455268368121;...
0.234726937385715 -3.67284750289918;...
0.0233895809473227 -1.64580325279074;...
0.580486875095951 -1.32703754874287;...
1.03620061490418 -5.65846162482610;...
0.109178065994015 -2.44262447245539;...
1.67734440189744 -19.2033203174615;...
0.793568366589840 -3.89663780862964;...
NaN NaN;...
0.381179723211034 -2.53080152927374;...
0.283377572690330 -0.222150372933123;...
NaN NaN;...
0.380836251403691 -7.63038214593229];

%% Download radiometry data
for i = 2:12
    try
        temp = table2array(webread([adress 'ch' num2str(i) '.csv']));
        data.rad(:,rad_ch(i-1)) = temp(:,2);
    end
end
data.t_rad = temp(:,1)*1e3;


%% Plot all channels in one picture
f = figure('visible', 'off');
% figure;
hold on;
for i = 2:12
    plot(data.t_rad, polyval(callibration(rad_ch(i-1),:), 1000*data.rad(:,rad_ch(i-1))));
end
  % ylim([0 50]);
   xlim([0 30]);
   xlabel('t, ms');
   ylabel('T_r_a_d, eV');
grid on;
hold off

saveas(f, 'icon-fig.png');
close(f)


%% Plot all channels separatly
f = figure('visible', 'off');
for i = 2:12
   subplot(3, 4, i-1);
   title(['CH' num2str(i-1)]);
   hold on;
   plot(data.t_rad, polyval(callibration(rad_ch(i-1),:), 1000*data.rad(:,rad_ch(i-1))));
   ylim([0 50]);
   xlim([0 30]);
   xlabel('t, ms');
   ylabel('T_r_a_d, eV');
   grid on;
end

saveas(f, 'channels.png');
close(f);
exit;
