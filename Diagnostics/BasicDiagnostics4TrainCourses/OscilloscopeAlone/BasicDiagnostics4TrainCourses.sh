BasePath=../../..;source $BasePath/Commons.sh

DAS="Oscilloscopes/RigolMSO5104-a/RemoteTrainingOsc"
DeviceList="$DAS"

function PostDischargeAnalysis
{
   GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    cp DAS_raw_data_dir/* .
    
    GenerateDiagWWWs $instrument $setup $DAS 
}
