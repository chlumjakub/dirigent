BASEDIR="../.."
source $BASEDIR/Commons.sh


diag_id="TrainingCourses"
setup_id="RigolB_RigolD"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Oscilloscopes/RigolMSO5204-b/RemoteTrainingOsc-b Oscilloscopes/RigolMSO5204-b/RemoteTrainingOsc-d"

DeviceList="$DAS"
