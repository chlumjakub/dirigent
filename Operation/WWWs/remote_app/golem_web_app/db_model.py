"""Database model for ORM"""


def model_factory(app):
    """MySQL database ORM mapping"""
    fallback = (None,) * 4      # db, Shot, AccessToken
    try:
        from flask_sqlalchemy import SQLAlchemy
    except ImportError:
        return fallback

    try:
        db = SQLAlchemy(app)
    except Exception:
        return fallback
    
    with app.app_context():
        db.metadata.reflect(db.engine) # default public schema
        db.metadata.reflect(db.engine, schema='remote')
        db.metadata.reflect(db.engine, schema='operation')

    class Shot(db.Model):
        '''ORM model representing rows of the shots table'''
        __tablename__ = 'remote.shots'

    class AccessToken(db.Model):
        '''ORM model representing available access tokens'''
        __tablename__ = 'remote.access_tokens'

    class Discharges(db.Model):
        __tablename__ = 'operation.discharges'
        
    return db, Shot, AccessToken, Discharges
