import io
import json
from flask import Flask, render_template, request, redirect, url_for, session, flash, request_started, Response, send_file
from hashlib import md5
import qrcode
import datetime
from wtforms import Form, Field, SelectField, StringField, DateTimeField, IntegerField, RadioField, DecimalField, SubmitField, validators
from unidecode import unidecode
from collections import OrderedDict
import logging
import base64
from flask_wtf import FlaskForm
from .dirigent_parser import parse_command, DischargeCommand, get_basic_config, get_session_config
from sqlalchemy import exc


page_nav = OrderedDict((
    ('intro', 'Introduction'),
    ('control_room', 'Simple Control Room'),
    ('full_control_room', 'Full Control Room'),
    ('status', 'Live'),
    ('results', 'Results'),
))


# main web app object
app = Flask("golem_web_app")
app.config.from_object('golem_web_app.secret_settings')  # This file should be next to this one
app.config['site_name'] = 'GOLEM remote'
app.config['page_nav'] = page_nav
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# set via FLASK_ENV env var
in_development = app.config.get('ENV', 'production') == 'development'

# extra logging
handler = logging.FileHandler('/tmp/golem_remote.log')
handler.setLevel(logging.DEBUG)
app.logger.addHandler(handler)
#app.logger.setLevel(logging.DEBUG)

#app.config['SQLALCHEMY_ECHO'] = True

# SQL model ORM
from .db_model import model_factory
db, Shot, AccessToken, Discharges = model_factory(app)



# recommended root URI: /remote/

user_attrs = ('identification', 'access_token')

@app.route('/login/', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        for key in user_attrs:
            session[key] = request.form[key]
        session['access_level'] = get_access_level(session['access_token'])
        if session['access_level']:
            flash('Access granted.')
            return redirect(url_for(request.form.get('next', 'index')))
        else:
            error = 'Invalid or expired access credentials. Try using the link or QR code you were provided (with the access token included in the URL) or request a new one.'
    defaults = {key: session.get(key) for key in user_attrs}
    defaults['next'] = request.args.get('next', 'index')
    defaults['access_level'] = session.get('access_level', 0)
    return render_template('login.html', error=error, **defaults)


@app.route('/logout/')
def logout():
    for key in user_attrs:
        if request.args.get(key) == 'yes':
            app.logger.debug('deleting session key %s' % key)
            del session[key]
    return redirect(url_for(request.args['next']))

debug_access_to_num = {'': 0,
                       'zero': 0,
                       'one': 1,
                       'two': 2,
                       'three': 3,
                       'four': 4,
                       'five': 5,
                       'six': 6,
                       'seven': 7,
                       'eight': 8,
                       'nine': 9,
                       'ten': 10}

def get_access_level(access_token=None):
    if access_token is None:
        access_token = session.get('access_token', '')
    if access_token in debug_access_to_num.keys() and in_development:
            return debug_access_to_num.get(access_token) or 99               # for development
    if app.config['NO_LOGIN_LOCAL_ACCESS'] and request.remote_addr.startswith('192.168.'):
        return app.config['NO_LOGIN_LOCAL_ACCESS']
    token = AccessToken.query.filter(AccessToken.data==access_token).first()
    if (token is not None
        and (token.expires is None or
             token.expires >= datetime.datetime.now().astimezone())):
        return token.level
    else:
        return 0

def generate_new_token(level=1, expires=None, expires_in=None, usage='temporary'):
    now = datetime.datetime.now().astimezone()
    data = md5((app.config['GENERATOR_TOKEN_SALT'] + str(now)).encode('utf-8')).hexdigest()[:32]
    if expires is None and expires_in is not None:
        expires = now + expires_in
    token = AccessToken(data=data, level=level, expires=expires, usage=usage, )
    db.session.add(token)
    db.session.commit()
    return token


def setup_access_level(sender, **extra):
    # copy any URL parameters to session
    for key in user_attrs:
        try:
            val = request.args[key]
        except KeyError:
            continue
        session[key] = val
    ident = session.get('identification')
    if ident == '_':            # special placeholder to erase
        del session['identification']
        ident = None
    if not ident:
        access_level = 0     # cannot access without identification
    else:
        access_level = get_access_level()
    session['access_level'] = access_level
    session.permanent = True

request_started.connect(setup_access_level, app)



@app.context_processor
def inject_active_dispatch():
    return dict(active_dispatch=request.path.strip('/'),
    )


@app.route('/')
def index():
    '''If introduction was skipped, go to control room, otherwise show introduction'''
    if request.cookies.get('skipIntroduction'):
        return redirect(url_for('control_room'))
    else:
        return redirect(url_for('intro'))


@app.route('/intro/')
def intro():
    '''Show introduction to fusion and the GOLEM tokamak'''
    return render_template('intro.html')



class RequestDischargeForm(FlaskForm):
    submit = SubmitField('Submit Discharge')
            
    
def getDischargeWTFParameterField(parameter, force_string = False):
    validators_arr = [validators.InputRequired()]
    if not force_string:
        if parameter.type == 'int':
            validators_arr.append(validators.NumberRange(parameter.min, parameter.max))
            return IntegerField(parameter.name,validators=validators_arr,default=parameter.parsed_value,
                                render_kw={"step": str(parameter.step)})
        
        elif parameter.type == 'float':
            validators_arr.append(validators.NumberRange(parameter.min, parameter.max))
            return DecimalField(parameter.name,validators=validators_arr,places=abs(parameter.step.as_tuple().exponent),
                        default=parameter.parsed_value, render_kw={"step": str(parameter.step)}) ## force step kw
            
        elif parameter.type == 'switch':
            if parameter.options_alt_text is not None and len(parameter.options_alt_text) == len(parameter.options):
                choices = tuple((opt,f"{opt} ({alt})") for opt, alt in zip(parameter.options, parameter.options_alt_text))
            else:
                choices = tuple((opt,opt) for opt in parameter.options)
            
            if len(parameter.options) < 5:
                return RadioField(parameter.name,validators=validators_arr, choices=choices,default=parameter.parsed_value) 
            else:
                return SelectField(parameter.name,validators=validators_arr, choices=choices,default=parameter.parsed_value) 
                
            
        elif parameter.type == 'waveform':
            validators_arr.append(validators.Regexp(r'^[\d \.,;\-\+]+$',message='Use numbers and ".,;-+" characters'))
            return StringField(parameter.name,validators=validators_arr, default=parameter.parsed_value)
        elif parameter.type == 'binary':
            validators_arr.append(validators.Regexp(r'^[01]+$', message='Only "1" and "0" are allowed.'))
            return StringField(parameter.name,validators=validators_arr, default=parameter.parsed_value)
        elif parameter.type == 'shot_no':
            validators_arr.append(validators.NumberRange(-1, 99999, message='Use valid shot number'))
            return IntegerField(parameter.name,validators=validators_arr,default=parameter.parsed_value)
    
    # use StringField as default     
    validators_arr.append(validators.Regexp(r'^[\w \_\-\+\.,;]+$', message='Only alfanumeric and "_+-.,;" are allowed.'))
    return StringField(parameter.name,validators=validators_arr, default=parameter.parsed_value)
    

## Get Allowed discharge params for simple control room
simple_discharge_command = parse_command('--comment \'\' ', use_config=get_basic_config())

# remove operation.discharge (will be automatically set to 'remote')
simple_discharge_command.subsystems.pop('operation.discharge')
simple_discharge_command.subsystems.pop('discharge')

req_params = [simple_discharge_command["infrastructure.workinggas"]["p_H"],
              simple_discharge_command["infrastructure.workinggas"]["S_gas"],
              simple_discharge_command["infrastructure.preionization"]["SW_main"],
              simple_discharge_command["infrastructure.preionization"]["S_device"],
              simple_discharge_command["infrastructure.bt_ecd"]["U_Bt"],
              simple_discharge_command["infrastructure.bt_ecd"]["U_cd"],
              simple_discharge_command["infrastructure.bt_ecd"]["t_cd"],
              simple_discharge_command["comment"]['']]

additional_subsys = [simple_discharge_command["infrastructure.positionstabilization"],
                    simple_discharge_command["infrastructure.biasingelectrode"]]

class SimpleRequestDischargeForm(FlaskForm):
    submit = SubmitField('Submit')

for param in req_params:
        form_field = getDischargeWTFParameterField(param)
        setattr(SimpleRequestDischargeForm,param.full_id_name, form_field)
    
for subsystem in additional_subsys:
    for param in subsystem.parameters.values():
        form_field = getDischargeWTFParameterField(param)
        setattr(SimpleRequestDischargeForm,param.full_id_name, form_field)


control_tabs_dict = OrderedDict([
    # key : (tab_name, description, label, param, recommended_val)
    ('intro', ('Introduction',
               'This web interface will walk you through the process of configuring a discharge in the GOLEM tokamak. All settable values are perfectly safe. Proceed through each step by setting the desired values and then clicking the <span class="text-primary">Next</span> button. You can always go to a specific step by clicking its tab.',
              None,
              [],
              None,

              
    )),
    ('gas', ('Working gas',
             'Set the pressure and type of the working gas from which the plasma is formed. Pressure must be high enough for plasma to form, but low enough for gas breakdown to occur.',
             'Gas type and pressure p<sub>WG</sub>',
             [simple_discharge_command["infrastructure.workinggas"]["p_H"], 
              simple_discharge_command["infrastructure.workinggas"]["S_gas"]],
             10
    )),
    ('preion', ('Preionization',
                'The neutral working gas must be first ionized in order to break down into a plasma.'
                'Using the <span class="text-danger">electron gun</span> will locally ionize the gas. Without any ionization, no plasma can form.',
                'Gas type and pressure p<sub>WG</sub>',
                [simple_discharge_command["infrastructure.preionization"]["SW_main"],
                 simple_discharge_command["infrastructure.preionization"]["S_device"]],
                None
    )),
    ('Bt', ('Magnetic field',
            'Set the voltage on the capacitors to be discharged into the <span class="text-muted">toroidal field coils</span>. '
            'The higher the voltage, the larger the <span class="text-success">magnetic field</span> <i>confining</i> the plasma.',
            'Capacitor voltage U<sub>B<sub>t</sub></sub>',
            [simple_discharge_command["infrastructure.bt_ecd"]["U_Bt"]],
            800
    )),
    ('CD', ('Electric field',
            'Set the voltage on the capacitors to be discharged into the <span class="text-primary">primary transformer winding</span>. '
            'The higher the voltage, the larger the <span class="text-success">electric field</span> <i>creating and heating</i> the plasma.',
            'Capacitor voltage U<sub>E<sub>t</sub></sub>',
            [simple_discharge_command["infrastructure.bt_ecd"]["U_cd"],
             simple_discharge_command["infrastructure.bt_ecd"]["t_cd"]],
            450
    )),
])


tabs =list(control_tabs_dict.keys()) + ['submit']
show_3D_args = {
    'True': True,
    'False': False,
}


@app.route('/control_room/')
def control_room():
    '''Display the control interface and the discharge queue'''
    if not session['access_level']:
        return redirect(url_for('login', next='control_room'))
        
    else:
        discharge_form = SimpleRequestDischargeForm()
        return render_template('control_room.html',
                            control_tabs_dict=control_tabs_dict, additional_subsys = additional_subsys,
                            tabs=tabs, discharge_form = discharge_form, zip = zip,
                            comment_field = simple_discharge_command["comment"][''],
                            showX3DOM=show_3D_args.get(request.args.get('showX3DOM', 'False'), False),
        )
    
    
    
MASTER = -1
VACUUM = -2

def get_shot_discharge_command(shot_no: int):
    '''
    Get Parset Discharge discharge Command
    shot_no: 
        -  0 == Last Discharge
        - -1 == Master Discharge
        - -2 == Vacuum Discharge 
    '''
    

    try:
        if shot_no == 0:
            query_result = db.session.query(Discharges.X_discharge_command).order_by(Discharges.shot_no.desc()).limit(1).scalar()
            
          
        elif shot_no not in [MASTER, VACUUM]:
            query_result = db.session.query(Discharges.X_discharge_command).filter(Discharges.shot_no == shot_no).scalar()

        else:
             query_result = None
    except (ValueError, TypeError) as e:
        app.logger.error("Cannot Query Discharge Command %s", e)
        query_result = None
        
    
    if query_result is None:
        query_result = '' 
    
    app.logger.warning("Query for X_discharge_command %s", query_result)
    
    config = get_session_config()
    if config is None:
        return None
    
    discharge_command = parse_command(query_result, use_config=config)
  

    if shot_no == VACUUM:
        discharge_command["infrastructure.bt_ecd"]["U_Bt"].parsed_value = 0
        discharge_command["infrastructure.bt_ecd"]["U_cd"].parsed_value = 0
        
        discharge_command["infrastructure.bt_ecd"]["U_Bt"].new_value = 0
        discharge_command["infrastructure.bt_ecd"]["U_cd"].new_value = 0


    # remove operation.discharge (will be automatically set to 'remote')
    discharge_command.subsystems.pop('operation.discharge')
    discharge_command.subsystems.pop('discharge')

    # remove comment
    discharge_command["comment"][''].parsed_value = ""

    return discharge_command
    
    
def get_last_session_shot_nums(filter = None):
    try:
        latest_session_id = db.session.query(db.func.max(Discharges.session_id)).scalar()
        latest_discharges = Discharges.query.filter_by(session_id=latest_session_id).order_by(Discharges.shot_no.desc())

        shot_nums = [discharge.shot_no for discharge in latest_discharges]
        app.logger.warning("Last session shot nums %r", shot_nums)
    except (ValueError, TypeError, exc.SQLAlchemyError) as e:
        app.logger.error("Cannot read shot nums in session %s", e)
        shot_nums = []
    return shot_nums

   

@app.route('/full_control_room/', methods=['POST', 'GET'])
@app.route('/full_control_room/<string:shot_no>', methods=['POST', 'GET'])
def full_control_room(shot_no: str = 'master'):
    if not session['access_level']:
        return redirect(url_for('login', next='full_control_room'))
    elif session['access_level'] < 1:
        return redirect(url_for('control_room'))
    
    shot_no = shot_no.lower()
    
    force_string_params = shot_no.endswith('_d')
    if force_string_params:
        shot_no = shot_no[:-2]
    
    shot_no = MASTER if isinstance(shot_no, str) and shot_no == 'master' else shot_no
    shot_no = VACUUM if isinstance(shot_no, str) and shot_no == 'vacuum' else shot_no
    shot_no =      0 if isinstance(shot_no, str) and not shot_no.isdecimal() else int(shot_no)
    
    session_shot_nums = get_last_session_shot_nums()
    
    
    discharge_command = get_shot_discharge_command(shot_no)    
    
    # full_control_room/<string:shot_no> switches view
    # so new FilledDischargeRequestFrorm is created
    class FilledDischargeRequestFrorm(RequestDischargeForm):
        pass
    
    
    submit_success = False
    
    if discharge_command is None:
        is_session = False
        dischage_form = FilledDischargeRequestFrorm()
    else:
        is_session = True

    
        for subsystem in discharge_command.subsystems.values():
            for param in subsystem.parameters.values():
                form_field = getDischargeWTFParameterField(param, force_string_params)
                setattr(FilledDischargeRequestFrorm,param.full_id_name, form_field)
                
        dischage_form = FilledDischargeRequestFrorm()
        
        
        if len(discharge_command.subsystems) > 0 and dischage_form.validate_on_submit():
            for subsystem in discharge_command.subsystems.values():
                for param in subsystem.parameters.values():
                    param.new_value = dischage_form[param.full_id_name].data
            
            if len(discharge_command.subsystems) > 0:
                submit_success = write_discharge_to_db(discharge_command)
            
    
    ## do not show invalid values in form
    shot_no = shot_no if shot_no >= 0 else 0
    return render_template('full_control_room.html', discharge_form = dischage_form, shot_no = shot_no,
                           force_string_params = force_string_params, submit_success = submit_success,
                           discharge_command = discharge_command, session_shot_nums= session_shot_nums,
                           is_session = is_session, zip=zip)

def write_discharge_to_db(discharge_command: DischargeCommand):
    discharge_params = ''
    if len(discharge_command.subsystems) > 0:
                discharge_params = discharge_command.assemble_subsystems(human_readable=False)
    
    app.logger.debug('discharge parameters request: %r', discharge_params)
    
    if Shot is None and in_development:
        return True, 1234
    kargs = dict()
    kargs['extra_params'] = discharge_params
    
    # add basic shot parameters to database (backward compatibility)
    if "infrastructure.bt_ecd" in discharge_command.subsystems:
        if "U_Bt" in discharge_command["infrastructure.bt_ecd"].parameters:
            kargs['ub'] = int(discharge_command["infrastructure.bt_ecd"]["U_Bt"].value)
        if "U_cd" in discharge_command["infrastructure.bt_ecd"].parameters:
            kargs['ucd']  = int(discharge_command["infrastructure.bt_ecd"]["U_cd"].value)
        if "t_cd" in discharge_command["infrastructure.bt_ecd"].parameters:
            kargs['tcd'] = int(discharge_command["infrastructure.bt_ecd"]["t_cd"].value)
            
    if "infrastructure.workinggas" in discharge_command.subsystems:
        if "p_H" in discharge_command["infrastructure.workinggas"].parameters:
            kargs['h2'] = int(discharge_command["infrastructure.workinggas"]["p_H"].value)
        if "S_gas" in discharge_command["infrastructure.workinggas"].parameters:
            kargs['gas'] = discharge_command["infrastructure.workinggas"]["S_gas"].value
    
    if "infrastructure.preionization" in discharge_command.subsystems \
        and "SW_main" in discharge_command["infrastructure.preionization"].parameters:
            kargs['ion'] = 1 if discharge_command["infrastructure.preionization"]["SW_main"].value == 'on' else 0
            
    if "infrastructure.positionstabilization" in discharge_command.subsystems:
        kargs['infrastructure_positionstabilization'] = discharge_command["infrastructure.positionstabilization"].assemble_parameters()
    else:
        kargs['infrastructure_positionstabilization'] = ''

    if "infrastructure.biasingelectrode" in discharge_command.subsystems:
        kargs['infrastructure_biasingelectrode'] = discharge_command["infrastructure.biasingelectrode"].assemble_parameters()
    else:
        kargs['infrastructure_biasingelectrode'] = ''

    if "comment" in discharge_command.subsystems:
            kargs['comment'] = discharge_command["comment"][''].value
    
    try:
        shot = Shot(**kargs) 
    except TypeError:
        app.logger.exception('Shot creation failed')
        return False, 0
    
    shot.ip = request.remote_addr
    shot.identification = session.get('identification', '')
    
    db.session.add(shot)
    db.session.commit()
    
    return True, shot.vshotno



@app.route('/discharge_request/', methods=['POST', 'GET'])
def discharge_request():
    discharge_form = SimpleRequestDischargeForm()
    
    #return render_template('request_test.html', discharge_form= discharge_form)
    if not discharge_form.validate_on_submit():
        return 'Invalid Discharge Request', 400
    
    discharge_params = ''
    if len(simple_discharge_command.subsystems) > 0 and discharge_form.validate_on_submit():
        for subsystem in simple_discharge_command.subsystems.values():
            for param in subsystem.parameters.values():
                
                # not all parameters are displayed in simple control room
                if hasattr(discharge_form,param.full_id_name):
                    param.new_value = discharge_form[param.full_id_name].data
            
    shot_db_success, vshotno = write_discharge_to_db(simple_discharge_command)
    
    if not shot_db_success:
        return 'Invalid parameters', 400
    
    if Shot is None and in_development:
        return json.dumps({
        'machineActive': True,
        'minutesToDischarge': 2,
        'vshotno': 1234,
    })
    
    preceding = Shot.query.filter(Shot.status < 2).count()
    response_json = json.dumps({
        'machineActive': True,  # TODO detect/save in g machine status
        'minutesToDischarge': (preceding + 1) * 2,
        'vshotno': vshotno,
    })
    return response_json        # TODO json encoding



@app.route('/status/')
def status():
    '''Display the current status of the GOLEM tokamak'''
    for_video_status = request.args.get('for_video_secret', '') == app.config.get('FOR_VIDEO_STATUS_TOKEN')
    video_status_id = app.config.get('VIDEO_STATUS_ID')
    if for_video_status:
        return render_template('status.html', for_video_status=True)
    elif session['access_level'] >= 1:
        if video_status_id:
            return render_template('video_status.html', video_id=video_status_id)
        else:
            return render_template('status.html', for_video_status=False)
    else:
        return render_template('video_status.html', video_id='Tt30kYSk0c4')


@app.route('/results/')
def results():
    '''Display results for the current user'''
    # TODO pagination
    if not session.get('identification'):
        return redirect(url_for('login', next='results'))

    if Shot is None and in_development:
        shots = []
    else:
        shots = Shot.query.filter(
            (Shot.identification == session.get('identification', ''))
            #& (Shot.shotno > 0) comment by PM to display shots in results even with null shotnumber
            ).order_by(Shot.date.desc())
    return render_template('results.html', shots=shots)


@app.route('/queue.json')
def queue():
    if Shot is None and in_development:
        shots = []
    else:
        shots = Shot.query.filter(Shot.status < 2).order_by(Shot.date.desc())
    shots_l = [s.__dict__ for s in shots]
    for s in shots_l:
        s.pop('_sa_instance_state', None)
        s['date'] = str(s['date'])
    ret = json.dumps({'queue': shots_l}, ensure_ascii=False)
    return Response(unidecode(ret), mimetype='application/json')


class GroupAccessGeneratorForm(Form):
    groupname = StringField('Group base name', [validators.Length(min=3, max=64)])
    n_groups = IntegerField('Number of groups', [validators.NumberRange(min=1)])
    event_date = DateTimeField('Date of the event', format='%Y-%m-%d')
    expire_after_n_days = IntegerField('Access expires n days the event date', [validators.NumberRange(min=1)])
    level = IntegerField('Access level', [validators.NumberRange(min=0)])
    admin_password = StringField('Access admin password')


@app.route('/generate_group_access', methods=['POST'])
def generate_group_access():
    form = GroupAccessGeneratorForm(request.form)
    if not form.validate():
        return "invalid POST data:\n{}".format(str(form.errors)), 400
    if form.admin_password.data != app.config['ACCESS_ADMIN_PASSWORD']:
        return "Wrong access admin password", 403
    expires = form.event_date.data + datetime.timedelta(days=form.expire_after_n_days.data)
    token = generate_new_token(level=form.level.data, usage=form.groupname.data, expires=expires)
    n_groups = form.n_groups.data
    groups_maxlen = len(str(n_groups))
    links = [url_for('control_room', identification='{} Group {:0{}d}'.format(
        form.groupname.data, i, groups_maxlen),
                     access_token=token.data, _external=True)
             for i in range(1, n_groups+1)]
    return '\n'.join(links)


@app.route('/generate_temporary_access')
def generate_temporary_access():
    passwd = request.args.get('access_admin_password', '')
    usage = request.args.get('usage', 'temporary')
    if passwd != app.config['ACCESS_ADMIN_PASSWORD']:
        return "Wrong access admin password", 403
    expires_in = datetime.timedelta(minutes=int(request.args.get('minutes', 30)))
    token = generate_new_token(level=request.args.get('level', 1),
                                     expires_in=expires_in, usage=usage)
    return redirect(url_for('qr_access_link', access_token=token.data, identification='_'))



@app.route('/qr_access_link.png')
def qr_access_link():
    # WARNING: requires SITE_NAME setting for absolute URL
    kwargs = {}
    for uat in user_attrs:
        v = request.args.get(uat)
        if v:
            kwargs[uat] = v
    link = url_for('control_room', _external=True, **kwargs)
    image = qrcode.make(link)
    stream = io.BytesIO()
    image.save(stream, 'PNG')
    stream.seek(0)
    img_data = base64.b64encode(stream.read())
    return render_template('qr_code.html', img_data=img_data, target_url=link)

