import json
import sys
from functools import partial
import pathlib
import argparse
from pathlib import Path
import jinja2


s = json.load(open('Bookmarks'))['roots']['bookmark_bar']['children']
global html_code
html_code = ''
html_code += '<div class="box"><ul class="directory-list">'
def read_bookmarks(s, level, dirname_base=''):
    global html_code
    book = s
    for bk in (book):
        if(level == 0 and bk['name'] in (['Golem', 'Experiments', 'Showroom'])):
            if('children' not in bk.keys()):
                html_code += f'<li> <a class="file" href={bk["url"]}>{bk["name"]}</a> '
                html_code += '</li>'
            else:
                dirname = dirname_base
                dirname += f'{bk["name"]}/'
                html_code += f'<li class="{dirname.replace(" ","")}">{bk["name"]}</strong>'
                html_code += f'<ul class="{dirname.replace(" ","")}">'
                read_bookmarks(bk['children'], level=level+1, dirname_base=dirname)    
                html_code += '</ul></li>'
        if(level != 0):
            if('children' not in bk.keys()):
                html_code += f'<li> <a class="file" href={bk["url"]}>{bk["name"]}</a> '
                html_code += '</li>'
            else:
                dirname = dirname_base
                dirname += f'{bk["name"]}/'
                html_code += f'<li class="{dirname.replace(" ","")}">{bk["name"]}</strong>'
                html_code += f'<ul class="{dirname.replace(" ","")}">'
                read_bookmarks(bk['children'], level=level+1, dirname_base=dirname)    
                html_code += '</ul></li>'
#     print(depth)
read_bookmarks(s, level=0)
html_code += '</ul></div>'


this_dir = Path.cwd()#pathlib.Path(__file__).parent.resolve()
jenv = jinja2.Environment(loader=jinja2.FileSystemLoader(str(this_dir)),
                          trim_blocks=True)
tpl = jenv.get_template('index_tpl.html')
with open('bookmarks.html', 'w') as fout:
    fout.write(tpl.render(data=html_code))
