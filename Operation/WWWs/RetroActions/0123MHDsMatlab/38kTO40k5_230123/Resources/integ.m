function [Y_int] = integ(Y, dx)
%����������� ���������, ���������� ������
 Y_int = zeros(size(Y));
 Y_int(1,:) = Y(1,:)*dx;
for i = 2:max(size(Y))
    Y_int(i,:) = Y_int(i-1,:) + Y(i,:).*dx;
end

end

