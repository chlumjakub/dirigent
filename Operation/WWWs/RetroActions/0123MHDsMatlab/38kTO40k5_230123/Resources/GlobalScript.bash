#Nutno volat http://golem.fjfi.cvut.cz/RetroActions/XY, a ne https:// (kvuli obrazkum)
# matlab nutno pocitat bez X: ssh golem@gm
#bash GlobalScript.bash 38kTO40k5doplneni "seq 39331 40500" true false
#bash GlobalScript.bash 39kSelection40k "39165 39317 39391 39773" true true
RunName=$1
shots=$2
DoTheRetroAction=$3 #true or false
Verbose=$4 #true or false

id=`basename $PWD`
#id=0123MHDs
BP=/golem/shots
diag=Diagnostics/MHDring_TM
cesta=/golem/database/www/RetroActions/$id/"$RunName"_`date '+%d%m%y'`
soubor=index.html
echo Je to zde: http://golem.fjfi.cvut.cz/RetroActions/$id/`basename $cesta`
logfile="$id"_"$RunName"_`date '+%d%m%y'`.log
echo;date|tee -a AllActions.log
echo $@ |tee -a AllActions.log

mkdir -p $cesta;cd $cesta
mkdir -p Resources
cp /golem/Dirigent/$diag/* ../*.* Resources/ 2>/dev/null
echo "Date (start):  `date`"|tee -a $logfile
echo $@ |tee -a $logfile
echo "============================="|tee -a $logfile


echo "<html><body><h2>\
$diag <a href=Resources>Retroaction</a> \
<a href=http://golem.fjfi.cvut.cz/RetroActions/$id>Base dir: $id</a>,  \
<a href=https://gitlab.com/golem-tokamak/dirigent/-/blob/master/$diag>Gitlab</a>\
</h2>" > $soubor; 
echo "Command line: bash ${BASH_SOURCE} $@<br/>" >> $soubor;
echo "Parameters: $shots . <a href=$logfile>Log file</a><br/>" >> $soubor;
echo "Date (start):  `date`" >> $soubor;
echo "<hr><table>" >> $soubor;
echo "<tr><th>Basic Diag</th><th>offset</th><th>MHD_activity</th><th>Spectrogram</th><th>q</th><th>animation</th><th>pictures</th><th>MHD link</th></tr>" >> $soubor;

function IndivShot ()
{
local ShotNo=$1
    { cmd=$(< $BP/$ShotNo/Production/Parameters/CommandLine); } 2>/dev/null;cmd=${cmd##*vacuum_shot=}; Vacuum_shot=${cmd%%\"*}
    if \
    [[ -d "$BP/$ShotNo/$diag" ]] && \
    [[ -f "$BP/$ShotNo/Production/Parameters/CommandLine" ]] && \
    [[ -d "$BP/$Vacuum_shot/$diag" ]] && \
    [[ -f "$BP/$ShotNo/Diagnostics/BasicDiagnostics/Results/b_plasma" ]] && \
    [[ $(< $BP/$ShotNo/Diagnostics/BasicDiagnostics/Results/b_plasma) != "0.000" ]] && \
    [[ $(< $BP/$ShotNo/Production/Parameters/CommandLine) =~ "vacuum_shot" ]] && \
    [[ ! $(< $BP/$ShotNo/Production/Parameters/CommandLine) =~ "vacuum_shot=\"" ]]; then 
        echo; echo -ne Dive into $ShotNo \@ |tee -a $logfile; date '+%H:%M:%S'|tee -a $logfile 
        cp ../DischargeScript.bash $BP/$ShotNo/$diag/
        cd $BP/$ShotNo/$diag
        bash DischargeScript.bash $diag $ShotNo $DoTheRetroAction $Verbose $id/"$RunName"_`date '+%d%m%y'`
        cd $OLDPWD
        cat $BP/$ShotNo/$diag/RetroRow.html >> $soubor; 
        echo -ne Emerge from $ShotNo \@ |tee -a $logfile; date '+%H:%M:%S'|tee -a $logfile
        echo "-------- $shots ----------------------"
    fi
}


for ShotNo in `$shots`; do 
    echo -en $ShotNo ..; 
    IndivShot $ShotNo
done


echo "</table><hr/>">> $soubor;
echo "Date (end):  `date`<br/>" >> $soubor;
echo "<h3><u>Global script:</u></h3><pre>">> $soubor;
cat ../GlobalScript.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g' >> $soubor;
echo "</pre><hr/><h3><u>Discharge script:</u></h3><pre>">> $soubor;
cat ../DischargeScript.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g' >> $soubor;
echo "</pre></body></html>" >> $soubor;
echo "Date (end):  `date`"|tee -a $logfile
echo "------------------------------"|tee -a $logfile
cd `dirname $cesta`
date|tee -a AllActions.log
echo "------------------------------"|tee -a AllActions.log

