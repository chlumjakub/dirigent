#Nutno volat http://golem.fjfi.cvut.cz/RetroActions/XY, a ne https:// (kvuli obrazkum)
# matlab nutno pocitat bez X: ssh golem@gm
#bash GlobalScript.bash 38kTO40k5doplneni "seq 39331 40500" true false
#bash GlobalScript.bash 39kSelection40k "39165 39317 39391 39773" true true
RunName=$1
shots=$2
DoTheRetroAction=$3 #true or false
Verbose=$4 #true or false


diag=Diagnostics/Interferometry


id=`basename $PWD`
BP=/golem/shots
cesta=/golem/database/www/RetroActions/$id/"$RunName"_`date '+%d%m%y'`
soubor=index.html
echo Je to zde: http://golem.fjfi.cvut.cz/RetroActions/$id/`basename $cesta`
echo;date|tee -a log
echo $@ |tee -a log

mkdir -p $cesta;cd $cesta
mkdir -p Resources
cp /golem/Dirigent/$diag/* ../*.* Resources/ 2>/dev/null
echo "Date (start):  `date`"|tee -a log
echo $@ |tee -a log
echo "============================="|tee -a log


echo "<html><body><h2>\
$diag <a href=Resources>Retroaction</a> \
<a href=http://golem.fjfi.cvut.cz/RetroActions/$id>Base dir: $id</a>,  \
<a href=https://gitlab.com/golem-tokamak/dirigent/-/blob/master/$diag>Gitlab</a>\
</h2>" > $soubor; 
echo "Command line: bash ${BASH_SOURCE} $@<br/>" >> $soubor;
echo "Date (start):  `date`<br/>" >> $soubor;
echo "Parameters: $shots" >> $soubor;
echo "<hr><table>" >> $soubor;
echo "<tr><th>Basic Diag</th><th>before</th><th>after</th><th>outputs</th></tr>" >> $soubor;

function IndivShot ()
{
local ShotNo=$1
    if \
    [[ -d "$BP/$ShotNo/$diag" ]] && \
    [[ -f "$BP/$ShotNo/Production/Parameters/CommandLine" ]] && \
    [[ -f "$BP/$ShotNo/Diagnostics/BasicDiagnostics/Results/b_plasma" ]] && \
    [[ $(< $BP/$ShotNo/Diagnostics/BasicDiagnostics/Results/b_plasma) != "0.000" ]]; then 
        echo; echo -ne Dive into $ShotNo \@ |tee -a log; date '+%H:%M:%S'|tee -a log 
        cp ../RetroDischargeScript.bash $BP/$ShotNo/$diag/
        cd $BP/$ShotNo/$diag
        bash RetroDischargeScript.bash $diag $ShotNo $DoTheRetroAction $Verbose
        cd $OLDPWD
        cat $BP/$ShotNo/$diag/RetroRow.html >> $soubor; 
        echo -ne Emerge from $ShotNo \@ |tee -a log; date '+%H:%M:%S'|tee -a log
        echo "-------- $shots ----------------------"
    fi
}


for ShotNo in `$shots`; do 
    echo -en $ShotNo ..; 
    IndivShot $ShotNo
done


echo "</table><hr/>">> $soubor;
echo "Date (end):  `date`<br/>" >> $soubor;
echo "<h3><u>Global script:</u></h3><pre>">> $soubor;
cat ../GlobalScript.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g' >> $soubor;
echo "</pre><hr/><h3><u>Discharge script:</u></h3><pre>">> $soubor;
cat ../RetroDischargeScript.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g' >> $soubor;
echo "</pre></body></html>" >> $soubor;
echo "Date (end):  `date`"|tee -a log
echo "------------------------------"|tee -a log
cd `dirname $cesta`
date|tee -a log
echo "------------------------------"|tee -a log

