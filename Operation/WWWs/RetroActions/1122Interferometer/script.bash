from=36710
to=39900
soubor=/golem/database/www/RetroActions/1122Interferometry/index.html; 
diag=Diagnostics/Interferometry

echo "<html><body><table>" > $soubor;

for i in `seq $from $to`; do echo $i:; 
if [[ -d "/golem/database/operation/shots/$i/$diag" ]] && [[ $(< /golem/shots/$i/Diagnostics/BasicDiagnostics/Results/b_plasma) != "0.000" ]]; then 
echo "<tr>
<td><a href=http://golem.fjfi.cvut.cz/shots/$i>$i</a><br>`cat /golem/database/operation/shots/$i/shot_date`<br>`cat /golem/database/operation/shots/$i/shot_time`</td>
<td><img src=http://golem.fjfi.cvut.cz/shots/$i/Diagnostics/BasicDiagnostics/graph.png></td>
<td><img src=http://golem.fjfi.cvut.cz/shots/$i/$diag/graph.png></td>
</tr>" >> $soubor; fi;done

echo "</table><hr/><pre>
`cat script.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g'`
</pre></body></html>" >> $soubor;
