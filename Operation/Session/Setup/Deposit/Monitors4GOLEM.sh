above=DVI-I-2-2;left=DVI-I-1-1;MODE=1920x1080;xrandr --output HDMI-1 --primary --mode $MODE --output $above --mode 1920x1080 --above HDMI-1 --rotate normal --output $left --mode 1920x1080 --left-of HDMI-1 --rotate right

# up to 1024
#above=DVI-I-2-2;left=DVI-I-1-1;MODE=1920x1080;xrandr --output HDMI-1 --primary --mode $MODE --output $above --mode 1920x1080 --above HDMI-1 --rotate right --output $left --mode 1920x1080 --left-of HDMI-1
# Small left monitor
# above=DVI-I-2-2;left=DVI-I-1-1;MODE=1920x1080;xrandr --output HDMI-1 --primary --mode $MODE --output $above --mode 1920x1080 --above HDMI-1 --rotate right --output $left --mode 1366x768 --left-of HDMI-1
# With ATENS
# above=DVI-I-3-3;left=DVI-I-2-2;aten=DVI-I-1-1;MODE=1920x1080;xrandr --output HDMI-1 --primary --mode $MODE --output $above --mode 1920x1080 --above HDMI-1 --rotate right --output $left --mode 1366x768 --left-of HDMI-1 --output $aten --mode $MODE --same-as HDMI-1 --rotate normal
