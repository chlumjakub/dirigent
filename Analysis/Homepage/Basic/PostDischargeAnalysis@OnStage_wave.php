<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
/*bring our bash variables to PHP*/
$config = parse_ini_file("../../../session.setup", true);
extract($config);
?>

<?php include 'IndividualParts/Functions.php'; ?>
<?php include 'IndividualParts/Head.php'; ?>
<body>
<?php include 'IndividualParts/Navigation.php'; ?>
<div class="div-grid-container" >
    <div>
    <?php include 'IndividualParts/DischargeHeading.php'; ?>

    <table><tbody><tr valign="top">
    <?php include 'IndividualParts/TechnologicalParameters.php'; ?>
    <?php include 'IndividualParts/PlasmaParameters.php'; ?>
    </tr></tbody></table>
    </div>
    <div style='text-align:center;'>
    <?php include 'IndividualParts/BasicDiagnostics.php'; ?>
    </div>
</div>
<?php include 'IndividualParts/OnStageDiagnostics.php'; ?>
<?php CurrentStatusAnnouncement("OFF STAGE postdischarge analysis","Finalization"); ?>

<?php include 'IndividualParts/Foot.php'; ?>
<?php include 'IndividualParts/SideBar.php'; ?>
</body></html>

