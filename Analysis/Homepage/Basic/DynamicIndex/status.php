<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');


while(True){
  try{
    $string = file_get_contents("https://golem.fjfi.cvut.cz/current_status.json");
    $json = json_decode($string, true);
    $pom1 = $json["time"];#date("h:i:sa");
    sleep(1);
    $string = file_get_contents("https://golem.fjfi.cvut.cz/current_status.json");
    $json = json_decode($string, true);
    $pom2 = $json["time"];#date("h:i:sa");
    if("$pom1" != "$pom2"){
      #echo "data: The server time is: {$time}\n\n";
      #echo "retry: 50\n\n";
      if($json["tokamak_state"] == "idle"){
        echo "data: <h2><font color='red'> Tokamak GOLEM status: </font> </h2><br>".
        "data: The server time is: {$json["time"]} <br>".
        "Voltage on UBt is:    {$json["U_Bt"]} V<br>".
        "Voltage on UCD is: {$json["U_CD"]} V<br>".
        "Chamber presure is: {$json["p_ch"]} mPa<br>".
        "Tokamak state is: {$json["tokamak_state"]} <br>".
        " <br>".
        "<h2> <font color='red'> Discharge is ready, press F5 </font> </h2>".
        "\n\n";
      }
      elseif($json["tokamak_state"] == "Sleep-downCall@Everywhere"){
        echo "data: <h2><font color='red'> Tokamak GOLEM status: </font> </h2><br>".
        "The server time is: {$json["time"]} {$time}<br>".
        "Voltage on UBt is: {$json["U_Bt"]} V<br>".
        "Voltage on UCD is: {$json["U_CD"]} V<br>".
        "Chamber presure is: {$json["p_ch"]} mPa<br>".
        "Tokamak state is: {$json["tokamak_state"]} <br>".
        " <br>".
        "<h2> <font color='red'> The GOLEM tokamak is offline... </font> </h2>".
        "\n\n";
      }
      else{
        echo "data: <h2><font color='red'> Tokamak GOLEM status: </font> </h2><br>".
        "The server time is: {$json["time"]} <br>".
        "Voltage on UBt is: {$json["U_Bt"]} V<br>".
        "Voltage on UCD is: {$json["U_CD"]} V<br>".
        "Chamber presure is: {$json["p_ch"]} mPa<br>".
        "Tokamak state is: {$json["tokamak_state"]} <br>".
        " <br>".
        "<h2> <font color='red'> The discharge is being prepared, please wait... </font> </h2>\n\n";
      }
    }
    ob_flush();
    flush();

  }
  catch (\Error $e){
      echo "data: error, json not available \n\n";
  }
}
?>
