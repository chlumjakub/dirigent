#!/bin/bash
psql_password=` cat /golem/production/psql_password`;

psql postgresql://golem:$psql_password@127.0.0.1/golem_database << EOF
UPDATE operation.discharges AS s
SET tag_valid = r."valid", post_comment = r."comment"
FROM production.requests AS r
WHERE (s.shot_no = r.shot_no) and (r.confirmed=true and r.done=false);
UPDATE production.requests as r set done = 't'
FROM operation.discharges as s
WHERE (r.shot_no=s.shot_no) and (s.tag_valid is not null and s.post_comment is not null);
EOF
